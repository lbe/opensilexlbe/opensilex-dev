/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.opensilex.front.vueOwlExtension.types.data;

import org.opensilex.core.ontology.Oeso;
import org.opensilex.front.vueOwlExtension.types.VueOntologyDataType;

/**
 *
 * @author vmigot
 */
public class VueSelect implements VueOntologyDataType {

    @Override
    public String getTypeUri() {
        return Oeso.select.getURI();
    }

    @Override
    public String getInputComponent() {
        return "opensilex-XSDSelectInput";
    }

    @Override
    public String getViewComponent() {
        return "opensilex-XSDRawView";
    }

    @Override
    public String getLabelKey() {
        return "datatypes.select";
    }

}
