/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.opensilex.front.vueOwlExtension.types.object;

import org.opensilex.front.vueOwlExtension.types.VueOntologyObjectType;

/**
 *
 * @author vmigot
 */
public class VueComponent implements VueOntologyObjectType {

    @Override
    public String getTypeUri() {
        return "http://opendata.inrae.fr/PO2/core/Component";
    }

    @Override
    public String getInputComponent() {
        return "opensilex-process-ComponentPropertySelector";
    }

    @Override
    public String getViewComponent() {
        return "opensilex-XSDUriView";
    }
}
