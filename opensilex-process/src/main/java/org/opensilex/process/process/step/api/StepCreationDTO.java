//******************************************************************************
//                          StepCreationDTO.java
// OpenSILEX - Licence AGPL V3.0 - https://www.gnu.org/licenses/agpl-3.0.en.html
// Copyright © INRA 2019
// Contact: emilie.fernandez@inrae.fr, anne.tireau@inrae.fr, pascal.neveu@inrae.fr
//******************************************************************************
package org.opensilex.process.process.step.api;

import org.opensilex.process.process.step.dal.StepModel;
import org.opensilex.sparql.model.time.InstantModel;
import org.opensilex.core.organisation.dal.facility.FacilityModel;
import org.opensilex.core.scientificObject.dal.ScientificObjectModel;
import java.time.OffsetDateTime;
import org.apache.commons.lang3.StringUtils;
import java.util.ArrayList;
import java.net.URI;
import java.util.List;
import java.util.Map;

/**
 * @author Emilie Fernandez
 */
public class StepCreationDTO extends StepDTO {
  

    public StepModel newModel() {

        StepModel model = new StepModel();
        model.setUri(getUri());
        model.setType(getType());
        model.setName(getName());
        if (!StringUtils.isEmpty(start)) {
            InstantModel instant = new InstantModel();
            instant.setDateTimeStamp(OffsetDateTime.parse(start));
            model.setStart(instant);
        }
        if (!StringUtils.isEmpty(end)) {
            InstantModel endInstant = new InstantModel();
            endInstant.setDateTimeStamp(OffsetDateTime.parse(end));
            model.setEnd(endInstant);
        }
        
        List<StepModel> stepAfterList = new ArrayList<>(after.size());
        after.forEach((stepAfterURI) -> {
            StepModel stepModel = new StepModel();
            stepModel.setUri(stepAfterURI);
            stepAfterList.add(stepModel);
        });
        model.setAfter(stepAfterList);

        List<StepModel> stepBeforeList = new ArrayList<>(before.size());
        before.forEach((stepBeforeURI) -> {
            StepModel stepModel = new StepModel();
            stepModel.setUri(stepBeforeURI);
            stepBeforeList.add(stepModel);
        });
        model.setBefore(stepBeforeList);

        model.setDescription(getDescription());

        List<FacilityModel> facilityList = new ArrayList<>(facilities.size());
        facilities.forEach((facilityUri) -> {
            FacilityModel facilityModel = new FacilityModel();
            facilityModel.setUri(facilityUri);
            facilityList.add(facilityModel);
        });
        model.setFacilities(facilityList);
        
        if (input != null) {
            List<ScientificObjectModel> inputList = new ArrayList<>(input.size());
            input.forEach((inputUri) -> {
                ScientificObjectModel soInputModel = new ScientificObjectModel();
                soInputModel.setUri(inputUri);
                inputList.add(soInputModel);
            });
            model.setInput(inputList);
        }
        
        if (output != null) {
            List<ScientificObjectModel> outputList = new ArrayList<>(output.size());
            output.forEach((outputUri) -> {
                ScientificObjectModel soOutputModel = new ScientificObjectModel();
                soOutputModel.setUri(outputUri);
                outputList.add(soOutputModel);
            });
            model.setOutput(outputList);
        }

        model.setEquipment(getEquipment());
        
        if (attributes != null ) {
            model.setAttributes(attributes);
         }

        return model;
    }

}
