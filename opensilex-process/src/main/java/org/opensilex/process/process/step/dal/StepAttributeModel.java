//******************************************************************************
//                          StepAttributeModel.java
// OpenSILEX - Licence AGPL V3.0 - https://www.gnu.org/licenses/agpl-3.0.en.html
// Copyright © INRA 2019
// Contact: emilie.fernandez@inrae.fr, anne.tireau@inrae.fr, pascal.neveu@inrae.fr
//******************************************************************************

package org.opensilex.process.process.step.dal;
import java.net.URI;
import java.util.Map;
import org.opensilex.nosql.mongodb.MongoModel;

/**
 *
 * @author Emilie Fernandez
 */
public class StepAttributeModel extends MongoModel {
      
    Map<String, String> attributes;

    public Map<String, String> getAttributes() {
        return attributes;
    }

    public void setAttributes(Map<String, String> attributes) {
        this.attributes = attributes;
    }

}
