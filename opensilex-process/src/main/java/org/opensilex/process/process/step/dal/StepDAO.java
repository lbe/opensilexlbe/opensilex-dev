//******************************************************************************
//                          ProcessDAO.java
// OpenSILEX - Licence AGPL V3.0 - https://www.gnu.org/licenses/agpl-3.0.en.html
// Copyright © INRA 2019
// Contact: emilie.fernandez@inrae.fr, anne.tireau@inrae.fr, pascal.neveu@inrae.fr
//******************************************************************************
package org.opensilex.process.process.step.dal;

import org.opensilex.security.account.dal.AccountModel;
import org.opensilex.sparql.service.SPARQLService;
import org.opensilex.process.ontology.PO2;
import java.net.URI;
import java.util.List;
import com.mongodb.client.MongoCollection;
import static com.mongodb.client.model.Filters.eq;

import org.apache.commons.lang3.StringUtils;
import org.opensilex.sparql.deserializer.SPARQLDeserializers;
import org.opensilex.sparql.service.SPARQLQueryHelper;
import org.opensilex.utils.OrderBy;
import org.opensilex.utils.ListWithPagination;
import org.apache.jena.arq.querybuilder.SelectBuilder;
import org.apache.jena.sparql.expr.Expr;
import org.apache.jena.arq.querybuilder.AskBuilder;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.graph.Node;
import org.apache.jena.graph.NodeFactory;
import org.apache.jena.sparql.syntax.ElementGroup;
import org.apache.jena.graph.Triple;
import org.apache.jena.sparql.core.Var;
import static org.opensilex.sparql.service.SPARQLQueryHelper.makeVar;
import org.opensilex.sparql.exceptions.SPARQLException;
import java.time.OffsetDateTime;
import org.opensilex.sparql.mapping.SPARQLClassObjectMapper;
import org.apache.jena.sparql.syntax.ElementFilter;
import org.apache.jena.vocabulary.AS;
import org.opensilex.nosql.mongodb.MongoDBService;
import org.opensilex.nosql.exceptions.NoSQLInvalidURIException;
import com.mongodb.client.model.IndexOptions;
import org.opensilex.nosql.mongodb.MongoModel;
import com.mongodb.client.model.Indexes;
import org.apache.commons.collections4.MapUtils;
import org.bson.Document;
import java.util.*;

/**
 *
 * @author Emilie Fernandez
 */
public class StepDAO {

    protected final SPARQLService sparql;
    protected final MongoDBService nosql;
    public static final String ATTRIBUTES_COLLECTION_NAME = "stepAttribute";

    public static final Var startTimeStampStepVar = SPARQLQueryHelper.makeVar(SPARQLClassObjectMapper.getTimeStampVarName(StepModel.START_FIELD));
    public static final Var endTimeStampStepVar = SPARQLQueryHelper.makeVar(SPARQLClassObjectMapper.getTimeStampVarName(StepModel.END_FIELD));

    public StepDAO(SPARQLService sparql, MongoDBService nosql) {
        this.sparql = sparql;
        this.nosql = nosql;
    }

    public MongoCollection<StepAttributeModel> getAttributesCollection() {
        return nosql.getDatabase().getCollection(ATTRIBUTES_COLLECTION_NAME, StepAttributeModel.class);
    }

    private StepAttributeModel getStoredAttributes(URI uri) {
        StepAttributeModel storedAttributes = null;
        try {
            storedAttributes = nosql.findByURI(StepAttributeModel.class, ATTRIBUTES_COLLECTION_NAME, uri);
        } catch (NoSQLInvalidURIException e) {
            // no attribute found, just continue since it's optional
        }
        return storedAttributes;
    }

    public void createIndexes() {
        IndexOptions unicityOptions = new IndexOptions().unique(true);

        MongoCollection<StepModel> attributeCollection = nosql.getDatabase().getCollection(ATTRIBUTES_COLLECTION_NAME, StepModel.class);
        attributeCollection.createIndex(Indexes.ascending(MongoModel.URI_FIELD), unicityOptions);
    }
    

    public StepModel createStep(StepModel instance) throws Exception {
        if (!MapUtils.isEmpty(instance.getAttributes())) {

            MongoCollection<StepAttributeModel> collection = getAttributesCollection();
            collection.createIndex(Indexes.ascending(MongoModel.URI_FIELD), new IndexOptions().unique(true));
            sparql.startTransaction();
            nosql.startTransaction();
            try {
                sparql.create(instance);

                StepAttributeModel attributeModel = new StepAttributeModel();
                attributeModel.setUri(instance.getUri());
                attributeModel.setAttributes(instance.getAttributes());
                collection.insertOne(nosql.getSession(), attributeModel);

                nosql.commitTransaction();
                sparql.commitTransaction();
            } catch (Exception ex) {
                nosql.rollbackTransaction();
                sparql.rollbackTransaction(ex);
            }
        } else {
            sparql.create(instance);
        }
        return instance;
    }

    public StepModel update(StepModel instance, AccountModel user) throws Exception {
        StepAttributeModel storedAttributes = getStoredAttributes(instance.getUri());
       
        if ((instance.getAttributes() == null || instance.getAttributes().isEmpty()) && storedAttributes == null) {
            sparql.update(instance);
        } else {
            nosql.startTransaction();
            sparql.startTransaction();
            sparql.update(instance);
            MongoCollection<StepAttributeModel> collection = getAttributesCollection();

            try {
                if (instance.getAttributes() != null && !instance.getAttributes().isEmpty()) {
                    StepAttributeModel model = new StepAttributeModel();
                    model.setUri(instance.getUri());
                    model.setAttributes(instance.getAttributes());
                    if (storedAttributes != null) {
                        collection.findOneAndReplace(nosql.getSession(), eq(MongoModel.URI_FIELD, instance.getUri()), model);
                    } else {
                        collection.insertOne(nosql.getSession(), model);
                    }
                } else {
                    collection.findOneAndDelete(nosql.getSession(), eq(MongoModel.URI_FIELD, instance.getUri()));
                }
                nosql.commitTransaction();
                sparql.commitTransaction();
            } catch (Exception ex) {
                nosql.rollbackTransaction();
                sparql.rollbackTransaction(ex);
            }

        }
        return instance;
    }

    public StepModel getStepByURI(URI instanceURI, AccountModel currentUser) throws Exception {
        StepModel step = sparql.getByURI(StepModel.class, instanceURI, currentUser.getLanguage());
        if (step != null) {
            StepAttributeModel storedAttributes = getStoredAttributes(step.getUri());
            if (storedAttributes != null) {
                step.setAttributes(storedAttributes.getAttributes());
            }
        }
        return step;
    }

    public List<StepModel> getList(List<URI> uri, AccountModel currentUser) throws Exception {
        List<StepModel> steps = null;
        if (sparql.uriListExists(StepModel.class, uri)) {
            steps = sparql.getListByURIs(StepModel.class, uri, currentUser.getLanguage());
        }
        if (steps != null) {
            for (StepModel step : steps) {
                if (step != null) {
                    StepAttributeModel storedAttributes = getStoredAttributes(step.getUri());
                    if (storedAttributes != null) {
                        step.setAttributes(storedAttributes.getAttributes());
                    }
                }
            }
        }
        return steps;
    }

    public ListWithPagination<StepModel> searchStep(AccountModel user, URI type, String name, OffsetDateTime start, OffsetDateTime end, List<URI> input, List<URI> output, Document attribute,
    List<OrderBy> orderByList, int page, int pageSize) throws Exception {
        final Set<URI> filteredUris;
        if (attribute != null) {
            filteredUris = filterURIsOnAttributes(attribute);
        } else {
            filteredUris = null;
        }

        if (attribute != null && (filteredUris == null || filteredUris.isEmpty())) {
            return new ListWithPagination<>(new ArrayList<>());
        } else {
            return sparql.searchWithPagination(
                StepModel.class,
                user.getLanguage(),
                (SelectBuilder select) -> {
                    Node stepGraph = sparql.getDefaultGraph(StepModel.class);
                    ElementGroup rootElementGroup = select.getWhereHandler().getClause();
                    ElementGroup multipleGraphGroupElem =  SPARQLQueryHelper.getSelectOrCreateGraphElementGroup(rootElementGroup, stepGraph);
                    
                    appendTypeFilter(select, type);
                    appendNameStepFilter(select, name);
                    appendTimeStepFilter(multipleGraphGroupElem, start, end);
                    appendInputStepFilter(select, input);
                    appendOutputStepFilter(select, output);
                    if (filteredUris != null) {
                        select.addFilter(SPARQLQueryHelper.inURIFilter(StepModel.URI_FIELD, filteredUris));
                    }
                },
                orderByList,
                page,
                pageSize
            );
        }
    }

    private Set<URI> filterURIsOnAttributes(Document attribute) {
        Document filter = new Document();
        if (attribute != null) {
            for (String key : attribute.keySet()) {
                filter.put("attribute." + key, attribute.get(key));
            }
        }
        return nosql.distinct(MongoModel.URI_FIELD, URI.class, ATTRIBUTES_COLLECTION_NAME, filter);
    }

    private void appendTypeFilter(SelectBuilder select, URI type) throws Exception {
        if (type != null) {
            select.addFilter(SPARQLQueryHelper.eq(StepModel.TYPE_FIELD, NodeFactory.createURI(SPARQLDeserializers.getExpandedURI(type.toString()))));
        }
    }

    private void appendNameStepFilter(SelectBuilder select, String name) throws Exception {
        if (!StringUtils.isEmpty(name)) {
            select.addFilter(SPARQLQueryHelper.regexFilter(StepModel.NAME_FIELD, name));
        }
    }

    protected void appendTimeStepFilter(ElementGroup processGraphGroupElem, OffsetDateTime start, OffsetDateTime end
    ) throws Exception {

        if (start == null && end == null) {
            return;
        }

        // startTimeStampVar and endTimeStampVar auto-added (in SELECT and WHERE) by the SPARQLClassQueryBuilder
        Expr dateRange = SPARQLQueryHelper.eventsIntervalDateRange(startTimeStampStepVar.getVarName(), start, endTimeStampStepVar.getVarName(), end);
        processGraphGroupElem.addElementFilter(new ElementFilter(dateRange));
    }

    private void appendInputStepFilter(SelectBuilder select, List<URI> input) throws Exception {
        if (input !=null && !input.isEmpty()) {
            addWhere(select, StepModel.URI_FIELD, PO2.hasInput, StepModel.INPUT_FIELD);
            select.addFilter(SPARQLQueryHelper.inURIFilter(StepModel.INPUT_FIELD, input));
        }
    }

    private void appendOutputStepFilter(SelectBuilder select, List<URI> output) throws Exception {
        if (output != null && !output.isEmpty()) {
            addWhere(select, StepModel.URI_FIELD, PO2.hasOutput, StepModel.OUTPUT_FIELD);
            select.addFilter(SPARQLQueryHelper.inURIFilter(StepModel.OUTPUT_FIELD, output));
        }
    }

    private static void addWhere(SelectBuilder select, String subjectVar, Property property, String objectVar) {
        select.getWhereHandler().getClause().addTriplePattern(new Triple(makeVar(subjectVar), property.asNode(), makeVar(objectVar)));
    }

    public void deleteStep(URI uri, AccountModel user) throws Exception {  
        nosql.startTransaction();
        sparql.startTransaction();
        sparql.delete(StepModel.class, uri);
        MongoCollection<StepAttributeModel> collection = getAttributesCollection();

        try {
            collection.findOneAndDelete(nosql.getSession(), eq(MongoModel.URI_FIELD, uri));
            nosql.commitTransaction();
            sparql.commitTransaction();
        } catch (Exception ex) {
            nosql.rollbackTransaction();
            sparql.rollbackTransaction(ex);
        }
    }
    

    public boolean isLinkedToProcess(StepModel step) throws SPARQLException {
        Var subject = makeVar("s");
        return sparql.executeAskQuery(
                new AskBuilder()
                        .addWhere(subject, PO2.hasStep, SPARQLDeserializers.nodeURI(step.getUri()))
        );
        
    }

    public List<RoleModel> searchRoles(String stringPattern, String lang ,List<OrderBy> orderByList) throws Exception{

        return sparql.search(
            RoleModel.class,
                lang,
                selectBuilder -> {
                    addRoleNameRegexFilter(selectBuilder, stringPattern);
                },
                orderByList
        );
    }

    private void addRoleNameRegexFilter(SelectBuilder selectBuilder, String stringPattern) {
        Expr regexFilter = SPARQLQueryHelper.regexFilter(RoleModel.NAME_FIELD, stringPattern);
        if (regexFilter != null) {
            selectBuilder.addFilter(regexFilter);
        }
    }

}