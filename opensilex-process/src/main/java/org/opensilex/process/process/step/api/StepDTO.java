//******************************************************************************
//                          StepDTO.java
// OpenSILEX - Licence AGPL V3.0 - https://www.gnu.org/licenses/agpl-3.0.en.html
// Copyright © INRA 2019
// Contact: emilie.fernandez@inrae.fr, anne.tireau@inrae.fr, pascal.neveu@inrae.fr
//******************************************************************************
package org.opensilex.process.process.step.api;

import io.swagger.annotations.ApiModelProperty;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import javax.validation.constraints.NotNull;
import org.opensilex.server.rest.validation.Required;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.opensilex.server.rest.validation.date.ValidOffsetDateTime;
import java.util.Map;

/**
 * @author Fernandez Emilie 
 */

public abstract class StepDTO {

    @JsonProperty("uri")
    protected URI uri;
    
    @JsonProperty("rdf_type")
    protected URI type;

    @JsonProperty("rdf_type_name")
    protected String typeLabel;

    @JsonProperty("name")
    protected String name;
    
    @JsonProperty("start")
    protected String start;
    
    @JsonProperty("end")
    protected String end;

    @JsonProperty("after")
    protected List<URI>  after;
    
    @JsonProperty("before")
    protected List<URI>  before;

    @JsonProperty("description")
    protected String description;

    @JsonProperty("facilities")
    protected List<URI> facilities = new ArrayList<>(); 

    @JsonProperty("input")
    protected List<URI> input = new ArrayList<>();

    @JsonProperty("output")
    protected List<URI> output = new ArrayList<>();

    @JsonProperty("equipment")
    protected URI equipment;

    @JsonProperty("attributes")
    protected Map<String, String> attributes;

    @ApiModelProperty(example = "http://opensilex.dev/set/process#Process3456")
    public URI getUri() {
        return uri;
    }

    public StepDTO setUri(URI uri) {
        this.uri = uri;
        return this;
    }

    @ApiModelProperty(example = "http://www.opensilex.org/process/ebo#Preheating")
    public URI getType() {
        return type;
    }

    public StepDTO setType(URI type) {
        this.type = type;
        return this;
    }

    public String getTypeLabel() {
        return typeLabel;
    }

    public StepDTO setTypeLabel(String typeLabel) {
        this.typeLabel = typeLabel;
        return this;
    }
    
    @ApiModelProperty(example = "dev:id/process/2022-02-20_bhp_2022-02-16_fermented-biowaste_35AI")
    public String getName() {
        return name;
    }

    public StepDTO setName(String name) {
        this.name = name;
        return this;
    }

    @ValidOffsetDateTime
    @ApiModelProperty(example = "2022-01-08T12:00:00+01:00")
    public String getStart() {
        return start;
    }

    public StepDTO setStart(String start) {
        this.start = start;
        return this;
    }

    @ValidOffsetDateTime
    @ApiModelProperty(example = "2022-01-08T12:00:00+01:00")
    public String getEnd() {
        return end;
    }

    public StepDTO setEnd(String end) {
        this.end = end;
        return this;
    }

    @ApiModelProperty(example = "http://example.com")
    public List<URI>  getAfter() {
        return after;
    }

    public StepDTO setAfter(List<URI>  after) {
        this.after = after;
        return this;
    }

    @ApiModelProperty(example = "http://example.com")
    public List<URI>  getBefore() {
        return before;
    }

    public StepDTO setBefore(List<URI>  before) {
        this.before = before;
        return this;
    }

    @ApiModelProperty(example = "description")
    public String getDescription() {
        return description;
    }

    public StepDTO setDescription(String description) {
        this.description = description;
        return this;
    }

    public List<URI> getFacilities() {
        return facilities;
    }
    
    public StepDTO setFacilities(List<URI> facilities) {
        this.facilities = facilities;
        return this;
    }

    @ApiModelProperty(example = "uri")
    public List<URI> getInput() {
        return input;
    }

    public StepDTO setInput(List<URI> input) {
        this.input = input;
        return this;
    }

    @ApiModelProperty(example = "uri")
    public List<URI> getOutput() {
        return output;
    }

    public StepDTO setOutput(List<URI> output) {
        this.output = output;
        return this;
    }

    @ApiModelProperty(example = "http://example.com")
    public URI getEquipment() {
        return equipment;
    }

    public StepDTO setEquipment(URI equipment) {
        this.equipment = equipment;
        return this;
    }

    @ApiModelProperty(example = "{volume=12}")
    public Map<String, String> getAttributes() {
        return attributes;
    }

    public StepDTO setAttributes(Map<String, String> attributes) {
        this.attributes = attributes;
        return this;
    }
}
