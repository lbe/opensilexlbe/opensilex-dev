//******************************************************************************
//                          ProcessModel.java
// OpenSILEX - Licence AGPL V3.0 - https://www.gnu.org/licenses/agpl-3.0.en.html
// Copyright © INRA 2019
// Contact: emilie.fernandez@inrae.fr, anne.tireau@inrae.fr, pascal.neveu@inrae.fr
//******************************************************************************
package org.opensilex.process.process.dal;

import org.opensilex.process.ontology.PO2;

import java.net.URI;
import java.util.List;
import java.time.LocalDate;
import org.apache.jena.vocabulary.RDFS;
import org.apache.jena.vocabulary.SKOS;
import org.opensilex.sparql.annotations.SPARQLIgnore;
import org.opensilex.sparql.annotations.SPARQLProperty;
import org.opensilex.sparql.annotations.SPARQLResource;
import org.opensilex.sparql.model.SPARQLLabel;
import org.opensilex.sparql.model.SPARQLNamedResourceModel;
import org.opensilex.uri.generation.ClassURIGenerator;
import org.opensilex.security.person.dal.PersonModel;

import org.opensilex.core.experiment.dal.ExperimentModel;
import org.opensilex.core.ontology.Oeso;
import java.util.stream.Collectors;
import org.opensilex.sparql.model.time.Time;
import org.opensilex.sparql.model.time.InstantModel;
import org.opensilex.process.process.step.dal.StepModel;

/**
 * @author Emilie Fernandez
 */
@SPARQLResource(
        ontology = PO2.class,
        resource = "Process",
        graph = ProcessModel.GRAPH,
        prefix = "process"
)
public class ProcessModel extends SPARQLNamedResourceModel<ProcessModel> implements ClassURIGenerator<ProcessModel> {

    public static final String GRAPH = "process";

    @SPARQLProperty(
        ontology = Oeso.class,
        property = "hasExperiment"
    )
    URI experiment;
    public static final String EXPERIMENT_FIELD = "experiment";


    @SPARQLProperty(
            ontology = Time.class,
            property = "hasBeginning",
            useDefaultGraph = false  // InstantModel stored in GRAPH
    )
    private InstantModel start;
    public static final String START_FIELD = "start";

    @SPARQLProperty(
            ontology = Time.class,
            property = "hasEnd",
            useDefaultGraph = false // InstantModel stored in GRAPH
    )
    private InstantModel end;
    public static final String END_FIELD = "end";

    @SPARQLProperty(
            ontology = Oeso.class,
            property = "hasScientificSupervisor"
    )
    List<PersonModel> scientificSupervisors;
    public static final String SCIENTIFIC_SUPERVISOR_FIELD = "scientificSupervisor";

    @SPARQLProperty(
            ontology = Oeso.class,
            property = "hasTechnicalSupervisor"
    )
    List<PersonModel> technicalSupervisors;
    public static final String TECHNICAL_SUPERVISOR_FIELD = "technicalSupervisor";

    @SPARQLProperty(
            ontology = RDFS.class,
            property = "comment"
    )
    String description;
    public static final String COMMENT_FIELD = "description";

    @SPARQLProperty(
        ontology = PO2.class,
        property = "hasStep",
        useDefaultGraph = false // PO2 graph default
    )
    List<StepModel> step;
    public static final String STEP_FIELD = "step";


    public URI getExperiment() {
        return experiment;
    }

    public void setExperiment(URI experiment) {
        this.experiment = experiment;
    }    

    public InstantModel getStart() {
        return start;
    }

    public void setStart(InstantModel start) {
        this.start = start;
    }

    public InstantModel getEnd() {
        return end;
    }

    public void setEnd(InstantModel end) {
        this.end = end;
    }

    public List<PersonModel> getScientificSupervisors() {
        return scientificSupervisors;
    }

    public void setScientificSupervisors(List<PersonModel> scientificSupervisors) {
        this.scientificSupervisors = scientificSupervisors;
    }

    public List<PersonModel> getTechnicalSupervisors() {
        return technicalSupervisors;
    }

    public void setTechnicalSupervisors(List<PersonModel> technicalSupervisors) {
        this.technicalSupervisors = technicalSupervisors;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<StepModel> getStep() {
        return step;
    }

    public void setStep(List<StepModel> step) {
        this.step = step;
    }

    public List<URI> getStepUris() {
        return this.step
                .stream()
                .map(StepModel::getUri)
                .collect(Collectors.toList());
    }
}
