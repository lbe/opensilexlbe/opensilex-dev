//******************************************************************************
//                          ProcessDAO.java
// OpenSILEX - Licence AGPL V3.0 - https://www.gnu.org/licenses/agpl-3.0.en.html
// Copyright © INRA 2019
// Contact: emilie.fernandez@inrae.fr, anne.tireau@inrae.fr, pascal.neveu@inrae.fr
//******************************************************************************
package org.opensilex.process.process.dal;

import org.opensilex.security.account.dal.AccountModel;
import org.opensilex.sparql.service.SPARQLService;
import org.opensilex.process.ontology.PO2;
import java.net.URI;
import java.util.List;
import java.util.ArrayList;
import org.apache.commons.lang3.StringUtils;
import org.opensilex.sparql.deserializer.SPARQLDeserializers;
import org.opensilex.sparql.service.SPARQLQueryHelper;
import org.opensilex.utils.OrderBy;
import org.opensilex.utils.ListWithPagination;
import org.apache.jena.arq.querybuilder.SelectBuilder;
import org.apache.jena.sparql.expr.Expr;
import org.apache.jena.arq.querybuilder.AskBuilder;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.graph.Node;
import org.apache.jena.sparql.syntax.ElementGroup;
import org.apache.jena.graph.Triple;
import org.apache.jena.sparql.core.Var;
import static org.opensilex.sparql.service.SPARQLQueryHelper.makeVar;
import org.opensilex.sparql.exceptions.SPARQLException;
import java.time.OffsetDateTime;
import org.opensilex.sparql.mapping.SPARQLClassObjectMapper;
import org.apache.jena.sparql.syntax.ElementFilter;
import org.opensilex.process.process.step.dal.StepModel;
import org.apache.commons.collections4.CollectionUtils;
import java.util.Collections;
import org.apache.jena.graph.NodeFactory;


/**
 *
 * @author Emilie Fernandez
 */
public class ProcessDAO {

    protected final SPARQLService sparql;
    public static final Var startTimeStampVar = SPARQLQueryHelper.makeVar(SPARQLClassObjectMapper.getTimeStampVarName(ProcessModel.START_FIELD));
    public static final Var endTimeStampVar = SPARQLQueryHelper.makeVar(SPARQLClassObjectMapper.getTimeStampVarName(ProcessModel.END_FIELD));
    public static final Var stepBeforeVar = SPARQLQueryHelper.makeVar(StepModel.BEFORE_FIELD);

    public ProcessDAO(SPARQLService sparql) {
        this.sparql = sparql;
    }

    public ProcessModel create(ProcessModel instance) throws Exception {
        String lang = null;
        List<StepModel> stepModel = sparql.getListByURIs(StepModel.class, instance.getStepUris(), lang);
        instance.setStep(stepModel);
        sparql.create(instance);
        return instance;
    }

    public ProcessModel update(ProcessModel instance, AccountModel user) throws Exception {
        List<StepModel> stepModels = sparql.getListByURIs(StepModel.class, instance.getStepUris(), user.getLanguage());
        instance.setStep(stepModels);
        sparql.update(instance);
        return instance;
    }

    public ProcessModel getProcessByURI(URI instanceURI) throws Exception {
        return sparql.getByURI(ProcessModel.class, instanceURI, null);
    }
    public ListWithPagination<ProcessModel> searchProcess(AccountModel user, URI type, String name, OffsetDateTime start, OffsetDateTime end, List<URI> step, List<OrderBy> orderByList, int page, int pageSize) throws Exception {
        
        return sparql.searchWithPagination(
            ProcessModel.class,
            user.getLanguage(),
            (SelectBuilder select) -> {
                Node processGraph = sparql.getDefaultGraph(ProcessModel.class);
                ElementGroup rootElementGroup = select.getWhereHandler().getClause();
                ElementGroup multipleGraphGroupElem =  SPARQLQueryHelper.getSelectOrCreateGraphElementGroup(rootElementGroup, processGraph);
               
                appendTypeFilter(select, type);
                appendNameProcessFilter(select, name);
                appendTimeFilter(multipleGraphGroupElem, start, end);
                appendStepProcessFilter(select, step);
            },
            orderByList,
            page,
            pageSize
        );
    }

    private void appendTypeFilter(SelectBuilder select, URI type) throws Exception {
        if (type != null) {
            select.addFilter(SPARQLQueryHelper.eq(ProcessModel.TYPE_FIELD, NodeFactory.createURI(SPARQLDeserializers.getExpandedURI(type.toString()))));
        }
    }

    private void appendNameProcessFilter(SelectBuilder select, String name) throws Exception {
        if (!StringUtils.isEmpty(name)) {
            select.addFilter(SPARQLQueryHelper.regexFilter(ProcessModel.NAME_FIELD, name));
        }
    }

    protected void appendTimeFilter(ElementGroup processGraphGroupElem, OffsetDateTime start, OffsetDateTime end
    ) throws Exception {

        if (start == null && end == null) {
            return;
        }

        // startTimeStampVar and endTimeStampVar auto-added (in SELECT and WHERE) by the SPARQLClassQueryBuilder
        Expr dateRange = SPARQLQueryHelper.eventsIntervalDateRange(startTimeStampVar.getVarName(), start, endTimeStampVar.getVarName(), end);
        processGraphGroupElem.addElementFilter(new ElementFilter(dateRange));
    }

    private void appendStepProcessFilter(SelectBuilder select, List<URI> step) throws Exception {
        if (step != null && !step.isEmpty()) {
            addWhere(select, ProcessModel.URI_FIELD, PO2.hasStep, ProcessModel.STEP_FIELD);
            select.addFilter(SPARQLQueryHelper.inURIFilter(ProcessModel.STEP_FIELD, step));
        }
    }

    public void deleteProcess(URI uri, AccountModel user) throws Exception {
        sparql.delete(ProcessModel.class, uri);
    }
    
    private static void addWhere(SelectBuilder select, String subjectVar, Property property, String objectVar) {
        select.getWhereHandler().getClause().addTriplePattern(new Triple(makeVar(subjectVar), property.asNode(), makeVar(objectVar)));
    }

}