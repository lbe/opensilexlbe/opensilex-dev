//******************************************************************************
//                          StepModel.java
// OpenSILEX - Licence AGPL V3.0 - https://www.gnu.org/licenses/agpl-3.0.en.html
// Copyright © INRA 2019
// Contact: emilie.fernandez@inrae.fr, anne.tireau@inrae.fr, pascal.neveu@inrae.fr
//******************************************************************************
package org.opensilex.process.process.step.dal;

import org.opensilex.process.ontology.PO2;
import java.net.URI;
import java.util.List;
import java.time.LocalDate;
import org.apache.jena.vocabulary.RDFS;
import org.opensilex.sparql.annotations.SPARQLIgnore;
import org.opensilex.sparql.annotations.SPARQLProperty;
import org.opensilex.sparql.annotations.SPARQLResource;
import org.opensilex.sparql.model.SPARQLLabel;
import org.opensilex.sparql.model.SPARQLNamedResourceModel;
import org.opensilex.uri.generation.ClassURIGenerator;
import org.opensilex.sparql.model.time.Time;
import org.opensilex.sparql.model.time.InstantModel;
import org.opensilex.core.scientificObject.dal.ScientificObjectModel;
import org.opensilex.process.process.dal.ProcessModel;
import org.opensilex.core.organisation.dal.facility.FacilityModel;
import org.opensilex.core.device.dal.DeviceModel;
import org.opensilex.core.ontology.Oeso;
import java.util.Map;

/**
 * @author Emilie Fernandez
 */
@SPARQLResource(
        ontology = PO2.class,
        resource = "Step",
        graph = ProcessModel.GRAPH,
        prefix = "step"
)
public class StepModel extends SPARQLNamedResourceModel<StepModel> implements ClassURIGenerator<StepModel> {

    @SPARQLProperty(
            ontology = Time.class,
            property = "hasBeginning",
            useDefaultGraph = false
    )
    private InstantModel start;
    public static final String START_FIELD = "start";

    @SPARQLProperty(
            ontology = Time.class,
            property = "hasEnd",
            useDefaultGraph = false
    )
    private InstantModel end;
    public static final String END_FIELD = "end";

    @SPARQLProperty(
        ontology = Time.class,
        property = "after"
    )
    List<StepModel> after;
    public static final String AFTER_FIELD = "after";

    @SPARQLProperty(
            ontology = Time.class,
            property = "before"
    )
    List<StepModel> before;
    public static final String BEFORE_FIELD = "before";

    @SPARQLProperty(
            ontology = RDFS.class,
            property = "comment"
    )
    String description;
    public static final String COMMENT_FIELD = "description";

    @SPARQLProperty(
        ontology = Oeso.class,
        property = "usesFacility"
    )
    List<FacilityModel> facilities;
    public static final String FACILITY_FIELD = "facilities";    

    @SPARQLProperty(
            ontology = PO2.class,
            property = "hasInput"
    )
    List<ScientificObjectModel> input;
    public static final String INPUT_FIELD = "input";

    @SPARQLProperty(
        ontology = PO2.class,
        property = "hasOutput"
    )
    List<ScientificObjectModel> output;
    public static final String OUTPUT_FIELD = "output";   

    @SPARQLProperty(
        ontology = Oeso.class,
        property = "usesEquipment"
    )
    URI equipment;
    public static final String EQUIPMENT_FIELD = "equipment";    

    Map<String, String> attributes;

    public InstantModel getStart() {
        return start;
    }

    public void setStart(InstantModel start) {
        this.start = start;
    }

    public InstantModel getEnd() {
        return end;
    }

    public void setEnd(InstantModel end) {
        this.end = end;
    }

    public List<StepModel> getAfter() {
        return after;
    }

    public void setAfter(List<StepModel> after) {
        this.after = after;
    }

    public List<StepModel> getBefore() {
        return before;
    }

    public void setBefore(List<StepModel> before) {
        this.before = before;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<FacilityModel> getFacilities() {
        return facilities;
    }

    public void setFacilities(List<FacilityModel> facilities) {
        this.facilities = facilities;
    }
    
    public List<ScientificObjectModel> getInput() {
        return input;
    }

    public void setInput(List<ScientificObjectModel> input) {
        this.input = input;
    }

    public List<ScientificObjectModel> getOutput() {
        return output;
    }

    public void setOutput(List<ScientificObjectModel> output) {
        this.output = output;
    }

    public URI getEquipment() {
        return equipment;
    }

    public void setEquipment(URI equipment) {
        this.equipment = equipment;
    }
  
    public Map<String, String> getAttributes() {
        return attributes;
    }

    public void setAttributes(Map<String, String> attributes) {
        this.attributes = attributes;
    }

}