//******************************************************************************
//                          RoleModel.java
// OpenSILEX - Licence AGPL V3.0 - https://www.gnu.org/licenses/agpl-3.0.en.html
// Copyright © INRA 2019
// Contact: emilie.fernandez@inrae.fr, anne.tireau@inrae.fr, pascal.neveu@inrae.fr
//******************************************************************************
package org.opensilex.process.process.step.dal;

import org.apache.jena.vocabulary.SKOS;
import org.opensilex.sparql.annotations.SPARQLProperty;
import org.opensilex.sparql.annotations.SPARQLResource;
import org.opensilex.sparql.model.SPARQLResourceModel;
import org.opensilex.process.ontology.BFO;
import org.opensilex.process.process.dal.ProcessModel;

/**
 * @author Emilie Fernandez
 */
@SPARQLResource(
        ontology = BFO.class,
        resource = "Role",
        graph = ProcessModel.GRAPH
)
public class RoleModel extends SPARQLResourceModel {

    @SPARQLProperty(
            ontology = SKOS.class,
            property = "prefLabel",
            ignoreUpdateIfNull = true,
            required = true
    )
    protected String name;
    public static final String NAME_FIELD = "name";

     public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
