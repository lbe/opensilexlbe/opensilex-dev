//******************************************************************************
//                          BFO.java
// OpenSILEX - Licence AGPL V3.0 - https://www.gnu.org/licenses/agpl-3.0.en.html
// Copyright © INRA 2019
// Contact: emilie.fernandez@inrae.fr, anne.tireau@inrae.fr, pascal.neveu@inrae.fr
//******************************************************************************
package org.opensilex.process.ontology;

import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Resource;
import org.opensilex.sparql.utils.Ontology;
/**
 * @author Emilie Fernandez
 */
public class BFO {
    public static final String DOMAIN = "http://purl.obolibrary.org/obo/";
    public static final String PREFIX = "BFO";
    /**
     * The namespace of the vocabulary as a string
     */
    public static final String NS = DOMAIN + "/";
    /**
     * The namespace of the vocabulary as a string
     *
     * @return namespace as String
     * @see #NS
     */
    public static String getURI() {
        return NS;
    }
    /**
     * namespace
     */
    public static final Resource NAMESPACE = Ontology.resource(NS);


    // ---- ROLE ----
    public static final Resource Role = Ontology.resource(NS, "BFO_0000023");
    public static final Property hasRole = Ontology.property(NS, "hasRole");
}