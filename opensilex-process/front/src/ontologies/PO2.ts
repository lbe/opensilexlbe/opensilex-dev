let PO2_URI = "http://opendata.inrae.fr/PO2/core";
let PO2_PREFIX = "PO2";

let getShortURI = (uri) => {
    return uri.replace(PO2_URI + "/", PO2_PREFIX + ":")
}

let checkURIs = (uri1, uri2) => {
    return getShortURI(uri1) == getShortURI(uri2);
}

let ontologies = {
    URI: PO2_URI,
    HAS_STEP_URI: PO2_URI + "/hasStep",
    HAS_INPUT_URI: PO2_URI + "/hasInput",
    HAS_OUTPUT_URI: PO2_URI + "/hasOutput",
    IS_COMPOSED_OF_URI: PO2_URI + "/isComposedOf",
    STEP_TYPE_URI: PO2_URI + "/Step",
    PROCESS_TYPE_URI: PO2_URI + "/Process",
    getShortURI: getShortURI,
    checkURIs: checkURIs
};

export default ontologies;