// graphConverter.ts
import Vue from 'vue';

export default function useGraphConverter(data: any[]) {
  const graph = Vue.observable({
    metadata: {
      style: {
        nodeStyles: {
          process: {
            width: 3,
            height: 3,
            strokeWidth: 0,
            displayLabel: true,
          },
          step: {
            fill: 'lightgrey',
            shape: 'rect',
          },
          output: {
            fill: 'lightgreen',
            shape: 'rect',
          },
          input: {
            fill: 'lightgreen',
            shape: 'rect',
          },
        },
      },
    },
    nodes: {},
    edges: [],
  });

  const getNode = (uri: string, label: string, group: number, classes: string[]) => {
    Vue.set(graph.nodes, uri, {
      metadata: { group, classes },
      label,
    });
  };

  const addEdge = (source: string, target: string, value: number) => {
    graph.edges.push({
      source,
      target,
      metadata: { value },
    });
  };

  const addInputOutputNodes = (uris: string[], group: number, classes: string[]) => {
    uris.forEach((uri) => {
      getNode(uri, '', group, classes);
    });
  };

  data.forEach((item) => {
    const { uri, rdf_type, name, before, input, output } = item;

    getNode(uri, name, 1, ['step']);

    if (before && before.length > 0) {
      before.forEach((beforeUri: string) => {
        addEdge(beforeUri, uri, 1);
      });
    }

    if (input && input.length > 0) {
      addInputOutputNodes(input, 2, ['input']);
      input.forEach((inputUri: string) => {
        addEdge(inputUri, uri, 10);
      });
    }

    if (output && output.length > 0) {
      addInputOutputNodes(output, 2, ['output']);
      output.forEach((outputUri: string) => {
        addEdge(uri, outputUri, 10);
      });
    }
  });

  return { graph };
}