/// <reference path="../../../opensilex-security/front/types/opensilex-security.d.ts" />
/// <reference path="../../../opensilex-core/front/types/opensilex-core.d.ts" />

import { ApiServiceBinder } from './lib';
import ProcessView from "./components/process/ProcessView.vue";
import ProcessForm from "./components/process/ProcessForm.vue";
import ProcessList from "./components/process/ProcessList.vue";
import ProcessDetail from "./components/process/ProcessDetail.vue";
import ProcessDetailView from "./components/process/ProcessDetailView.vue";
import StepDetail from "./components/process/step/StepDetail.vue";
import StepForm from "./components/process/step/StepForm.vue";
import StepList from "./components/process/step/StepList.vue";
import ProcessStepList from "./components/process/step/ProcessStepList.vue";
import StepSelector from "./components/process/step/StepSelector.vue";
import ComponentSelector from "./components/process/step/ComponentSelector.vue";
import StepView from "./components/process/step/StepView.vue";
import StepChart from "./components/process/step/StepChart.vue";
import StepTypes from './components/process/step/StepTypes.vue';

export default {
    install(Vue, options) {
        ApiServiceBinder.with(Vue.$opensilex.getServiceContainer());
    },
    components: {
        "opensilex-process-ProcessView": ProcessView,
        "opensilex-process-ProcessForm": ProcessForm,
        "opensilex-process-ProcessList": ProcessList,
        "opensilex-process-ProcessDetail": ProcessDetail,
        "opensilex-process-ProcessDetailView": ProcessDetailView,
        "opensilex-process-StepDetail": StepDetail,
        "opensilex-process-StepForm": StepForm,
        "opensilex-process-StepList": StepList,
        "opensilex-process-ProcessStepList": ProcessStepList,
        "opensilex-process-StepSelector": StepSelector,
        "opensilex-process-ComponentSelector": ComponentSelector,
        "opensilex-process-StepView": StepView,
        "opensilex-process-StepChart": StepChart,
        "opensilex-process-StepTypes" : StepTypes,
    },
    lang: {
        "fr": require("./lang/process-fr.json"),
        "en": require("./lang/process-en.json")
    }
};
