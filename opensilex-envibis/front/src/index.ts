/// <reference path="../../../opensilex-security/front/types/opensilex-security.d.ts" />
/// <reference path="../../../opensilex-core/front/types/opensilex-core.d.ts" />

// import { ApiServiceBinder } from './lib';
import EnvibisFooterComponent from "./components/layout/EnvibisFooterComponent.vue";
import EnvibisMenuComponent from "./components/layout/EnvibisMenuComponent.vue";
import EnvibisLoginComponent from "./components/layout/EnvibisLoginComponent.vue";
import EnvibisHeaderComponent from "./components/layout/EnvibisHeaderComponent.vue";

export default {
    install(Vue, options) {
    //     ApiServiceBinder.with(Vue.$opensilex.getServiceContainer());
    },
    components: {
        "opensilex-envibis-EnvibisFooterComponent": EnvibisFooterComponent,
        "opensilex-envibis-EnvibisMenuComponent": EnvibisMenuComponent,
        "opensilex-envibis-EnvibisLoginComponent": EnvibisLoginComponent,
        "opensilex-envibis-EnvibisHeaderComponent": EnvibisHeaderComponent
    },
    lang: {
        "fr": require("./lang/envibis-fr.json"),
        "en": require("./lang/envibis-en.json"),
    }
};
