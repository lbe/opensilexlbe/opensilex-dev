// //******************************************************************************
// //                          OdinModule.java
// // OpenSILEX LBE- Licence AGPL V3.0 - https://www.gnu.org/licenses/agpl-3.0.en.html
// // Copyright © INRAE 2020
// // Contact: emilie.fernandez@inrae.fr, virginie.rossard@inrae.fr, eric.latrille@inrae.fr
// //******************************************************************************
// package org.opensilex.odin.cli;

// import org.opensilex.odin.OdinConfig;
// import org.opensilex.odin.OdinModule;
// import java.sql.Connection;
// import java.sql.Statement;
// import java.sql.ResultSet;
// import java.sql.ResultSetMetaData;
// import java.sql.DriverManager;
// import java.util.Properties;
// import org.opensilex.cli.AbstractOpenSilexCommand;
// import org.opensilex.cli.HelpOption;
// import org.opensilex.cli.OpenSilexCommand;
// import org.opensilex.sparql.service.SPARQLService;
// import org.opensilex.sparql.service.SPARQLServiceFactory;
// import org.slf4j.Logger;
// import org.slf4j.LoggerFactory;
// import picocli.CommandLine;
// import picocli.CommandLine.Command;
// import java.util.ArrayList;
// import java.util.List;
// import java.util.Date;
// import java.util.HashMap;
// import java.util.List;
// import java.util.Map;
// import org.opensilex.utils.ListWithPagination;

// import org.opensilex.core.data.dal.DataDAO;
// import org.opensilex.core.data.dal.DataModel;
// import org.opensilex.core.data.api.DataCreationDTO;

// import org.opensilex.nosql.mongodb.MongoDBService;
// import org.opensilex.fs.service.FileStorageService;
// import org.opensilex.core.data.dal.DataProvenanceModel;
// import org.opensilex.core.variable.dal.VariableModel;

// import java.time.LocalDateTime;
// import java.time.OffsetDateTime;
// import java.time.ZoneOffset;
// import java.time.ZonedDateTime;
// import java.time.format.DateTimeFormatter;
// import java.time.format.DateTimeParseException;
// import org.opensilex.server.rest.validation.DateFormat;
// import java.net.URI;


// /**
//  *
//  * @author vmigot
//  * @author efernandez
//  */
// @Command(
//         name = "odin",
//         header = "Subcommand to group OpenSILEX LBE - Odin operations"
// )
// public class OdinCommands extends AbstractOpenSilexCommand implements OpenSilexCommand {

//     /**
//      * Class Logger.
//      */
//     private final static Logger LOGGER = LoggerFactory.getLogger(OdinCommands.class);

//     private MongoDBService nosql;
//     private SPARQLService sparql;
//     private FileStorageService fs;

//     @Command(
//             name = "update",
//             header = "Update Odin data",
//             description = "Read new data in Odin and insert them into OpenSilex"
//     )
//     public void update(
//             @CommandLine.Mixin HelpOption help
//     ) throws Exception {
//         OdinConfig odinConfig = getOpenSilex().getModuleConfig(OdinModule.class, OdinConfig.class);
//         String url = "jdbc:postgresql://" + odinConfig.host() + ":" + odinConfig.port() + "/" + odinConfig.database();
//         Properties props = new Properties();
//         props.setProperty("user", odinConfig.username());
//         props.setProperty("password", odinConfig.password());
//         Connection conn = DriverManager.getConnection(url, props);

//         SPARQLServiceFactory sparqlServiceFactory = getOpenSilex().getServiceInstance(SPARQLService.DEFAULT_SPARQL_SERVICE, SPARQLServiceFactory.class);
//         SPARQLService sparql = sparqlServiceFactory.provide();
//         try {
//       // TODO use "conn" to request odin data with SQL

//       Statement state = conn.createStatement();
//       ResultSet result = state.executeQuery("SELECT channel, raw.json ->> 'value' AS value, raw.json ->> 'timestamp' as timestamp, calibrations.json ->> 'unit' as unit, calibrations.sensor FROM raw LEFT JOIN calibrations on raw.channel = calibrations.sensor ORDER BY raw.created_at DESC limit 10");

//       //split column channel
//       String exp;
//       String prov;
//       String sensor;
//       final String SEPARATOR = "/";
//       String value;
//       String inputDate;
//       LocalDateTime dateTimeUTC = null;
//       String offset = null;
//       DataModel model = new DataModel();

//       URI varUri = new URI("http://opensilex.dev/set/variables#variable.test_test");
//       DataProvenanceModel provenance = new DataProvenanceModel();
//       URI provURI = new URI("http://opensilex.dev/provenance/1599748876284");
//       provenance.setUri(provURI);
  
//       //test conn
//       ResultSetMetaData resultMeta = result.getMetaData();
//       System.out.println("\n**********************************");
//       for(int i = 1; i <= resultMeta.getColumnCount(); i++)
//         System.out.print("\t" + resultMeta.getColumnName(i).toUpperCase() + "\t *");
//       System.out.println("\n**********************************");
         
//       //result
//       while(result.next()){         
//         //result channel  
//         String res = result.getString("channel");
//           String cutWord[] = res.split(SEPARATOR);
//           exp = cutWord[0];
//           prov = cutWord[1];
//           sensor = cutWord[3];

//         // result data
//         value = result.getString("value");
//         inputDate = result.getString("timestamp");
//         // varUri = result.getString("unit");

//         System.out.println("Nom exp : " + exp + ", Provenance : " + prov + ", Capteur : " + sensor + ", Données : " + value + ", Date : " + inputDate + ", variable : " + varUri);

//          // TODO use "sparql" with other modules DAO and models to insert data into opensilex

//          //convert provenance
//         //  ProvenanceDAO provDAO = new ProvenanceDAO(sparql);
//         //  provLabel = provDAO.findUriAndLabelsByLabel(prov);
//         //  for (String uri : provLabel.keySet()) {
//         //   provUri = uri;
//         //  }

//          //convert var
//         // VariableDAO varDAO = new VariableDAO(sparql);
//         // VariableModel variable = varDAO.get(uri);

//         // model.setScientificObjects(null);
//         // model.setVariable(varUri);
//         // model.setProvenance(provenance);
        
//         // OffsetDateTime ost = OffsetDateTime.parse(inputDate);
//         // dateTimeUTC = ost.withOffsetSameInstant(ZoneOffset.UTC).toLocalDateTime();
//         // offset = ost.getOffset().toString();
                         
//         // model.setDate(dateTimeUTC);
//         // model.setTimezone(offset);
       
//         // model.setValue(value);
//          }
//         // DataDAO dao = new DataDAO(nosql, sparql, fs);
//         // System.out.println("dto: " + model.getVariable());
//         // System.out.println("value: " + model.getValue());
//         // System.out.println("prov: " + model.getProvenance());
//         // System.out.println("date: " + dateTimeUTC);
//         // System.out.println("utc: " + offset);

        
//         // dao.create(model);
  
//         result.close();
//         state.close();

//         } finally {
//             sparqlServiceFactory.dispose(sparql);
//             if (conn != null) {
//                 conn.close();
//             }
//         }
//     }
// }
