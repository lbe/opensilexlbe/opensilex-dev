//******************************************************************************
//                          EnvibisConfig.java
// ENVIBIS - Licence AGPL V3.0 - https://www.gnu.org/licenses/agpl-3.0.en.html
// Copyright © INRAE 2020
// Contact: emilie.fernandez@inrae.fr, virginie.rossard@inrae.fr, eric.latrille@inrae.fr
//******************************************************************************
package org.opensilex.envibis;

import org.opensilex.config.ConfigDescription;

/**
 *
 * @author efernandez
 */
public interface OdinConfig {

    @ConfigDescription(
            value = "Odin database host",
            defaultString = "147.99.69.162"
    )
    public String host();

    @ConfigDescription(
            value = "Odin database port",
            defaultInt = 5432
    )
    public int port();

    @ConfigDescription(
            value = "Odin database user",
            defaultString = "odin"
    )
    public String username();

    @ConfigDescription(
            value = "Odin database password",
            defaultString = "odin42"
    )
    public String password();

    @ConfigDescription(
            value = "Odin database name",
            defaultString = "odin"
    )
    public String database();
}
