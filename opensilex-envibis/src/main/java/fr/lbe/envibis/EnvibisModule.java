//******************************************************************************
//                          EnvibisModule.java
// ENVIBIS - Licence AGPL V3.0 - https://www.gnu.org/licenses/agpl-3.0.en.html
// Copyright © INRAE 2020
// Contact: emilie.fernandez@inrae.fr, virginie.rossard@inrae.fr, eric.latrille@inrae.fr
//******************************************************************************
package org.opensilex.envibis;

import java.util.List;
import org.apache.jena.riot.Lang;
import org.opensilex.OpenSilexModule;
import org.opensilex.sparql.SPARQLConfig;
import org.opensilex.sparql.SPARQLModule;
import org.opensilex.sparql.extensions.OntologyFileDefinition;
import org.opensilex.sparql.extensions.SPARQLExtension;

/**
 *
 * @author efernandez
 */
public class EnvibisModule extends OpenSilexModule implements SPARQLExtension {

    @Override
    public Class<?> getConfigClass() {
        return OdinConfig.class;
    }

    @Override
    public String getConfigId() {
        return "odin"; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<OntologyFileDefinition> getOntologiesFiles() throws Exception {
        List<OntologyFileDefinition> list = SPARQLExtension.super.getOntologiesFiles();
        list.add(new OntologyFileDefinition(
                "http://opendata.inrae.fr/ebo#",
                "ontologies/ebo.owl",
                Lang.RDFXML,
                "ebo"
        ));

        return list;
    }

    // @Override
    // public void install(boolean reset) throws Exception {
    //     LOGGER.info("Insert default components");
    //     insertDefaultComponents();
    // }

    // private void insertDefaultComponents() throws URISyntaxException, OpenSilexModuleNotFoundException, IOException, SPARQLException {
    //     try {

    //         SPARQLServiceFactory factory = getOpenSilex().getServiceInstance(SPARQLService.DEFAULT_SPARQL_SERVICE, SPARQLServiceFactory.class);
    //         SPARQLService sparql = factory.provide();

    //         SPARQLConfig sparqlConfig = getOpenSilex().getModuleConfig(SPARQLModule.class, SPARQLConfig.class);
    //         String graph = sparql.getDefaultGraphURI(ScientificObjectModel.class).toString();
    //         OntologyFileDefinition ontologyDef = new OntologyFileDefinition(
    //                 graph,
    //                 "ontologies/components.ttl",
    //                 Lang.TTL,
    //                 null
    //         );

    //         InputStream ontologyStream = new FileInputStream(ClassUtils.getFileFromClassArtifact(getClass(), ontologyDef.getFilePath()));
    //         sparql.loadOntology(ontologyDef.getUri(), ontologyStream, ontologyDef.getFileType());
    //         ontologyStream.close();
        
    //     } catch (Exception ex) {
    //         LOGGER.error("Error while insering default components", ex);
    //     } 
        
    // }

    
}
