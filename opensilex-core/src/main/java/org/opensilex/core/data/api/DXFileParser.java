package org.opensilex.core.data.api;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Paths;
import org.opensilex.fs.service.FileStorageService;
import java.nio.file.Files;


/**
 * @author efernandez A utility class for parse DX spectrum file 
 */


 public class DXFileParser {
    private final FileStorageService fileService;

    public DXFileParser(FileStorageService fileService) {
        this.fileService = fileService;
    }

    public void parseDXfile(String inputFilePath) throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(inputFilePath));
        String line;
        String currentSampleName = null;
        String currentTime = null;
        StringBuilder currentContent = new StringBuilder();
        String FS_FILE_PREFIX = "datafile/spectres";

        while ((line = reader.readLine()) != null) {
            if (line.startsWith("##TITLE=")) {
                if (currentContent.length() > 0 && currentSampleName != null) {
                    java.nio.file.Path tempFile = Files.createTempFile("jdx_", ".dx");
                    try (FileWriter writer = new FileWriter(tempFile.toFile())) {
                        writer.write(currentContent.toString());
                    }
                    String filename = String.format("jdx_%s_%s.dx", currentSampleName, currentTime);
                    java.nio.file.Path filePath = Paths.get(FS_FILE_PREFIX, filename);
                    fileService.writeFile(FS_FILE_PREFIX, filePath, tempFile.toFile());
                    Files.deleteIfExists(tempFile);
                    currentContent = new StringBuilder();
                }
                currentSampleName = line.split("=")[1].trim();
            } else if (line.startsWith("##TIME=")) {
                currentTime = line.split("=")[1].trim();
            }
            currentContent.append(line).append("\n");
        }

        reader.close();

        if (currentContent.length() > 0 && currentSampleName != null) {
            java.nio.file.Path tempFile = Files.createTempFile("jdx_", ".dx");
            try (FileWriter writer = new FileWriter(tempFile.toFile())) {
                writer.write(currentContent.toString());
            }
            String filename = String.format("jdx_%s_%s.dx", currentSampleName, currentTime);
            java.nio.file.Path filePath = Paths.get(FS_FILE_PREFIX, filename);
            fileService.writeFile(FS_FILE_PREFIX, filePath, tempFile.toFile());
            Files.deleteIfExists(tempFile);
        }
    }
}
