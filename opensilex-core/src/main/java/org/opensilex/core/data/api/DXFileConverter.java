package org.opensilex.core.data.api;

import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.awt.*;
import org.jfree.chart.*;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.data.xy.XYSeries;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.xy.XYSeriesCollection;
import javax.swing.*;
import java.util.StringTokenizer;
import javax.imageio.ImageIO;
import org.jfree.chart.axis.NumberAxis;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
/**
 * @author efernandez A utility class for convert DX spectrum file to picture
 */
public class DXFileConverter {

    private static DXFileConverter _INSTANCE;

    public static DXFileConverter getInstance() throws IOException {
        if (_INSTANCE == null) {
            _INSTANCE = new DXFileConverter();
        }
        return _INSTANCE;
    }

    public byte[] convertDXToImage(byte[] dxData) throws IOException {
        String dxString = new String(dxData, StandardCharsets.UTF_8);

        Pattern firstXPattern = Pattern.compile("##FIRSTX=(\\d+\\.?\\d*)");
        Pattern lastXPattern = Pattern.compile("##LASTX=(\\d+\\.?\\d*)");
        Pattern deltaXPattern = Pattern.compile("##DELTAX=(\\d+\\.?\\d*)");
        Pattern factorYPattern = Pattern.compile("##YFACTOR=(.*)");
        Pattern titlePattern = Pattern.compile("##TITLE=(.*)");
        Pattern unitXPattern = Pattern.compile("##XUNITS=(.*)");
        Pattern unitYPattern = Pattern.compile("##YUNITS=(.*)");
        Pattern xyDataPattern = Pattern.compile("##XYDATA=(.*?)##END=", Pattern.DOTALL);

        double firstX = 0.0;
        double lastX = 0.0;
        double deltaX = 0.0;
        double factorY = 0.0;
        String title = "";
        String unitX = "";
        String unitY = "";
        String xyData = "";

        Matcher firstXMatcher = firstXPattern.matcher(dxString);
        if (firstXMatcher.find()) {
            firstX = Double.parseDouble(firstXMatcher.group(1));
        }

        Matcher lastXMatcher = lastXPattern.matcher(dxString);
        if (lastXMatcher.find()) {
            lastX = Double.parseDouble(lastXMatcher.group(1));
        }

        Matcher deltaXMatcher = deltaXPattern.matcher(dxString);
        if (deltaXMatcher.find()) {
            deltaX = Double.parseDouble(deltaXMatcher.group(1));
        }

        Matcher factorYMatcher = factorYPattern.matcher(dxString);
        if (factorYMatcher.find()) {
            factorY = Double.parseDouble(factorYMatcher.group(1));
        }

        Matcher titleMatcher = titlePattern.matcher(dxString);
        if (titleMatcher.find()) {
            title = titleMatcher.group(1).trim();
        }

        Matcher unitXMatcher = unitXPattern.matcher(dxString);
        if (unitXMatcher.find()) {
            unitX = unitXMatcher.group(1).trim();
        }

        Matcher unitYMatcher = unitYPattern.matcher(dxString);
        if (unitYMatcher.find()) {
            unitY = unitYMatcher.group(1).trim();
        }

        Matcher xyDataMatcher = xyDataPattern.matcher(dxString);
        if (xyDataMatcher.find()) {
            xyData = xyDataMatcher.group(1).trim();
        }

        BufferedImage graphImage = generateGraphImage(firstX, deltaX, title, unitX, unitY, xyData, factorY);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ImageIO.write(graphImage, "png", baos);
        byte[] imageData = baos.toByteArray();

        baos.close();

        return imageData;
    }

    private BufferedImage generateGraphImage(double firstX, double deltaX, String title, String unitX, String unitY, String xyData, double factorY) {
        XYSeries series = new XYSeries("Spectrum");
        String[] lines = xyData.split("\n");

        for (String line : lines) {
            String[] values = line.trim().split("\\s+");
            
            if (values.length >= 2) {
                double x = Double.parseDouble(values[0]);
                for (int i = 1; i < values.length; i++) {
                    double y = Double.parseDouble(values[i]) * factorY;
                    series.add(x, y);
                    x += deltaX;
                }
            }
        }
        XYSeriesCollection dataset = new XYSeriesCollection(series);
    
        JFreeChart chart = ChartFactory.createXYLineChart(
                title,                 
                unitX,         
                unitY,         
                dataset,      
                PlotOrientation.VERTICAL,
                true,               
                true,             
                false        
        );

        chart.setBackgroundPaint(Color.WHITE);
        chart.getTitle().setPaint(Color.BLACK);
        chart.getRenderingHints().put(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        chart.getRenderingHints().put(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);

        NumberAxis yAxis = new NumberAxis();
        DecimalFormatSymbols symbols = new DecimalFormatSymbols();
        symbols.setDecimalSeparator('.');
        DecimalFormat format = new DecimalFormat("0.##", symbols);
        yAxis.setNumberFormatOverride(format);
        yAxis.setLabel(unitY); 
        chart.getXYPlot().setRangeAxis(yAxis);

        int width = 400;
        int height = 300;
        BufferedImage graphImage = chart.createBufferedImage(width, height);

        return graphImage;
    }
}