import { ApiServiceBinder } from './lib';

let components = {};

import ApplicationView from './components/applications/ApplicationView.vue';
components["opensilex-data-analysis-ApplicationView"] = ApplicationView;

import ApplicationList from './components/applications/ApplicationList.vue';
components["opensilex-data-analysis-ApplicationList"] = ApplicationList;

import ViewEmbedApplication from './components/applications/ViewEmbedApplication.vue';
components["opensilex-data-analysis-ViewEmbedApplication"] = ViewEmbedApplication;

import ApplicationForm from './components/applications/ApplicationForm.vue';
components["opensilex-data-analysis-ApplicationForm"] = ApplicationForm;

import RCall from './components/r/RCall.vue';
components["opensilex-data-analysis-RCall"] = RCall;

export default {
    install(Vue, options) {
        ApiServiceBinder.with(Vue.$opensilex.getServiceContainer());
    },
    components: components,
    lang: {
        "fr": require("./lang/data-analysis-fr.json"),
        "en": require("./lang/data-analysis-en.json"),
    }
};