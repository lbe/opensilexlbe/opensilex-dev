- [Changelog](#changelog)
  - [\[2.6.0\]](#260)
  - [\[2.5.0\]](#250)
  - [\[2.4.1\]](#241)
  - [\[2.4.0\]](#240)

# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [2.6.0]

- Upgrade shinyproxy version to 3.1.1
- Add compatibility to version 1.2.8 of OpenSILEX
- Add programming language information
- Update home page

## [2.5.0]

- Upgrade shinyproxy version to 3.1.0
- Add compatibility to version 1.2.6 of OpenSILEX


## [2.4.1]

- Downgrade shinyproxy version to 2.6.1

## [2.4.0]

- Update shinyproxy version to 3.2.0
- Add compatibility to version 1.0.2 of OpenSILEX
