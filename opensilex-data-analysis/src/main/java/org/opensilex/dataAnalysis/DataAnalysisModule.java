package org.opensilex.dataAnalysis;

import java.util.List;
import org.apache.jena.riot.Lang;
import org.opensilex.OpenSilexModule;
import org.opensilex.fs.service.FileStorageService;
import org.opensilex.security.extensions.LoginExtension;
import org.opensilex.server.Server;
import org.opensilex.server.extensions.APIExtension;
import org.opensilex.server.extensions.ServerExtension;
import org.opensilex.sparql.extensions.OntologyFileDefinition;
import org.opensilex.sparql.extensions.SPARQLExtension;
import org.opensilex.sparql.service.SPARQLService;
import org.opensilex.sparql.service.SPARQLServiceFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Data analysis opensilex module implementation
 */
public class DataAnalysisModule extends OpenSilexModule implements APIExtension, LoginExtension, SPARQLExtension, ServerExtension {

    private final static Logger LOGGER = LoggerFactory.getLogger(DataAnalysisModule.class);

    @Override
    public Class<?> getConfigClass() {
        return DataAnalysisConfig.class;
    }

    @Override
    public String getConfigId() {
        return "data-analysis";
    }

    @Override
    public List<OntologyFileDefinition> getOntologiesFiles() throws Exception {
        List<OntologyFileDefinition> list = SPARQLExtension.super.getOntologiesFiles();
        list.add(new OntologyFileDefinition(
                "https://www.opensilex.org/vocabulary/oeso/analysis#",
                "ontologies/oeso-analysis.owl",
                Lang.RDFXML,
                "oeso-analysis"
        ));

        return list;
    }

    @Override
    public void initServer(Server server) throws Exception {
        new Thread(() -> {
            try {
                DataAnalysisConfig config = (DataAnalysisConfig) this.getConfig();
                SPARQLServiceFactory factory = getOpenSilex().getServiceInstance(SPARQLService.DEFAULT_SPARQL_SERVICE, SPARQLServiceFactory.class);
                SPARQLService sparql = factory.provide();

                FileStorageService fs = getOpenSilex().getServiceInstance(FileStorageService.DEFAULT_FS_SERVICE, FileStorageService.class);

                config.scientificAppServer().reload(sparql, fs);
            } catch (Exception ex) {
                LOGGER.error(ex.getMessage(), ex);
            }
        }).start();
    }


    @Override
    public void shutDownServer(Server server) throws Exception {
        DataAnalysisConfig config = (DataAnalysisConfig) this.getConfig();
        config.scientificAppServer().stopServer();
    }

}
