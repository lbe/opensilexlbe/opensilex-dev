//******************************************************************************
//                      ErrorResponse.java
// OpenSILEX - Licence AGPL V3.0 - https://www.gnu.org/licenses/agpl-3.0.en.html
// Copyright © INRA 2019
// Contact: vincent.migot@inra.fr, anne.tireau@inra.fr, pascal.neveu@inra.fr
//******************************************************************************
package org.opensilex.dataAnalysis.server.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import javax.ws.rs.core.Response.Status;
import org.opensilex.server.response.JsonResponse;
import org.opensilex.server.response.MetadataDTO;
import org.opensilex.server.response.PaginationDTO;

/**
 * <pre>
 * This class represents an HTTP information response (Status depend of the response ) in JSON.
  * Otherwise response status is defined by constructor parameter.
 *
 * Produced JSON:
 * {
 *      metadata: {
 *          pagination: {
 *              pageSize: 0,
 *              currentPage: 0,
 *              totalCount: 0,
 *              totalPages: 0
 *          },
 *          status: [],
 *          datafiles: []
 *      },
 *      result: {
 *          title: Title of the information
 *          message: Message of the information
 *      }
 * }
 * </pre>
 * 
 * @see org.opensilex.server.response.InformationDTO
 * @author Arnaud Charleory
 */
@JsonInclude(Include.NON_NULL)
public class InformationResponse extends JsonResponse<InformationDTO> {

    /**
     * Constructor
     * @param status
     * @param title
     * @param message
     */
    public InformationResponse(Status status, String title, String message) {
        super(status);
        metadata = new MetadataDTO(new PaginationDTO());
        result = new InformationDTO(title, message);
    }

}
