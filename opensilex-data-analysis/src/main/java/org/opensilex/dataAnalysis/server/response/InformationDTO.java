//******************************************************************************
//                           ErrorDTO.java
// OpenSILEX - Licence AGPL V3.0 - https://www.gnu.org/licenses/agpl-3.0.en.html
// Copyright © INRAE 2020
// Contact: arnaud.charleroy@inra.fr, anne.tireau@inra.fr, pascal.neveu@inra.fr
//******************************************************************************
package org.opensilex.dataAnalysis.server.response;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <pre>
 * This class define an information DTO used by {@org.opensilex.dataAnalysis.server.response}
 * It's defined by a title and a message .
 * </pre>
 *
 * @see org.opensilex.dataAnalysis.server.response
 * @author Arnaud Charleroy
 */
@ApiModel
public class InformationDTO {

    /**
     * Title of the information.
     */
    @ApiModelProperty(value = "Title of the information", example = "Information")
    public final String title;

    /**
     * Message of the information.
     */
    @ApiModelProperty(value = "Message of the information", example = "Status ok")
    public final String message; 

    public InformationDTO(String title, String message) {
        this.title = title;
        this.message = message;
    }

   
}
