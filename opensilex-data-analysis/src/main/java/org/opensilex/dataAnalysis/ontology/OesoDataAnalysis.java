//******************************************************************************
// OpenSILEX - Licence AGPL V3.0 - https://www.gnu.org/licenses/agpl-3.0.en.html
// Copyright © INRA 2019
// Contact: vincent.migot@inra.fr, anne.tireau@inra.fr, pascal.neveu@inra.fr
//******************************************************************************
package org.opensilex.dataAnalysis.ontology;

import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Resource;
import org.opensilex.sparql.utils.Ontology;

/**
 * @author Vincent MIGOT
 */
public class OesoDataAnalysis {

    public static final String DOMAIN = "http://www.opensilex.org/vocabulary/oeso/analysis";

    public static final String PREFIX = "analysis";

    /**
     * The namespace of the vocabulary as a string
     */
    public static final String NS = DOMAIN + "#";

    /**
     * The namespace of the vocabulary as a string
     *
     * @return namespace as String
     * @see #NS
     */
    public static String getURI() {
        return NS;
    }

    /**
     * Vocabulary namespace
     */
    public static final Resource NAMESPACE = Ontology.resource(NS);

    // ---- PROPERTIES ---- 
    public static final Property hasApplicationFile = Ontology.property(NS, "hasApplicationFile");
    public static final Property hasProgrammingLanguage = Ontology.property(NS, "hasProgrammingLanguage");

    // ---- VARIABLES ----
    public static final Resource ScientificApplication = Ontology.resource(NS, "ScientificApplication");

}
