/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.opensilex.dataAnalysis;

import org.opensilex.config.ConfigDescription;
import org.opensilex.dataAnalysis.opencpu.OpencpuServer;
import org.opensilex.dataAnalysis.shinyProxy.ShinyProxyServer;

/**
 *
 * @author Arnaud Charleroy
 */
public interface DataAnalysisConfig {

    @ConfigDescription(
        value = "ShinyProxyServerDocker config"
    )
    public ShinyProxyServer scientificAppServer();
    
    @ConfigDescription(
        value = "OpencpuServer config"
    )
    public OpencpuServer rServer();
    

}
