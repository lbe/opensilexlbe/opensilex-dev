//******************************************************************************
//                                ScientificAppDAO.java
// SILEX-PHIS
// Copyright © INRA 2019
// Creation date: 15 sept. 2019
// Contact: arnaud.charleroy@inrae.fr, anne.tireau@inrae.fr, pascal.neveu@inrae.fr
//******************************************************************************
package org.opensilex.dataAnalysis.dal;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import org.apache.jena.arq.querybuilder.SelectBuilder;
import org.apache.jena.sparql.expr.Expr;
import org.opensilex.fs.service.FileStorageService;
import org.opensilex.sparql.deserializer.SPARQLDeserializers;
import org.opensilex.sparql.service.SPARQLQueryHelper;
import org.opensilex.sparql.service.SPARQLService;
import org.opensilex.utils.ListWithPagination;
import org.opensilex.utils.OrderBy;

/**
 * ScientificAppDAO Manage Scientific applications in R or Python
 *
 * @author Arnaud Charleroy
 */
public class ScientificAppDAO {

    protected final SPARQLService sparql;

    protected final FileStorageService fs;

    public final static String FS_DATANALYSIS_PREFIX = "dataAnalysis";

    public ScientificAppDAO(SPARQLService sparql, FileStorageService fs) {
        this.fs = fs;
        this.sparql = sparql;
    }

    public ScientificAppModel create(ScientificAppModel instance, File file) throws Exception {
        
        try {
            sparql.create(instance);
            if(file != null){
                fs.writeFile(FS_DATANALYSIS_PREFIX, instance.getUri(), file);
            }
            sparql.commitTransaction();
        } catch (IOException e) {
            sparql.rollbackTransaction(e);
        } 
        return instance;
    }

    public ScientificAppModel update(ScientificAppModel instance) throws Exception {
        sparql.update(instance);
        return instance;
    }

    public void delete(URI instanceURI) throws Exception {
        sparql.delete(ScientificAppModel.class, instanceURI);
        fs.delete(FS_DATANALYSIS_PREFIX, new URI(SPARQLDeserializers.getExpandedURI(instanceURI)));
    }

    public ScientificAppModel get(URI instanceURI) throws Exception {
        return sparql.getByURI(ScientificAppModel.class, instanceURI, null);
    }

    
    public byte[] getFile(URI uri) throws Exception { 
        return fs.readFileAsByteArray(FS_DATANALYSIS_PREFIX, new URI(SPARQLDeserializers.getExpandedURI(uri))); 
    }
    public ListWithPagination<ScientificAppModel> search(
            String name,
            String programmingLanguage,
            List<OrderBy> orderByList,
            Integer page, Integer pageSize) throws Exception {
        return sparql.searchWithPagination(ScientificAppModel.class,
                null,
                (SelectBuilder select) -> {
                    // TODO implements filters
                    appendFilters(name,programmingLanguage, select);

                },
                orderByList,
                page,
                pageSize
        );
    }

    public List<ScientificAppModel> getAll() throws Exception {
        return sparql.search(ScientificAppModel.class, null);
    }

    /**
     * Append FILTER or VALUES clause on the given {@link SelectBuilder} for
     * each non-empty simple attribute ( not a {@link List} from the
     * {@link FactorSearchDTO}
     *
     * @param name name search attribute
     * @param select search query
     * @throws java.lang.Exception can throw an exception
     * @see SPARQLQueryHelper the utility class used to build Expr
     */
    protected void appendFilters(String name, String programmingLanguage, SelectBuilder select) throws Exception {

        List<Expr> exprList = new ArrayList<>();

        // build regex filters
        if (name != null && !name.equals("")) {
            exprList.add(SPARQLQueryHelper.regexFilter(ScientificAppModel.NAME_FIELD, name));
        }
        if (programmingLanguage != null && !programmingLanguage.equals("")) {
            exprList.add(SPARQLQueryHelper.regexFilter(ScientificAppModel.HAS_PROGRAMMING_LANGUAGE_FIELD, programmingLanguage));
        }

        for (Expr filterExpr : exprList) {
            select.addFilter(filterExpr);
        }
    }
}
