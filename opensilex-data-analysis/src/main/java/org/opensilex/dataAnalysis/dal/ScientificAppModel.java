//******************************************************************************
//                                ScientificAppComment.java
// SILEX-PHIS
// Copyright © INRA 2019
// Creation date: 9 sept. 2019
// Contact: arnaud.charleroy@inrae.fr, anne.tireau@inrae.fr, pascal.neveu@inrae.fr
//******************************************************************************
package org.opensilex.dataAnalysis.dal;

import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDate;
import java.util.ArrayList;
import javax.xml.bind.DatatypeConverter;
import org.apache.jena.vocabulary.DCTerms;
import org.apache.jena.vocabulary.RDFS;
import org.opensilex.dataAnalysis.ontology.OesoDataAnalysis;
import org.opensilex.sparql.annotations.SPARQLProperty;
import org.opensilex.sparql.annotations.SPARQLResource;
import org.opensilex.sparql.model.SPARQLResourceModel;
import org.opensilex.uri.generation.ClassURIGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * ScientificAppComment Describe a Scientific application
 *
 * @author Arnaud Charleroy
 */
@SPARQLResource(
        ontology = OesoDataAnalysis.class,
        resource = "ScientificApplication",
        graph = "shinyApps",
        prefix = "ScientificApplication"
)
public class ScientificAppModel extends SPARQLResourceModel implements ClassURIGenerator<ScientificAppModel> {

    final static Logger LOGGER = LoggerFactory.getLogger(ScientificAppModel.class);

    /**
     * Rdf applicationFileUri
     */
//    @SPARQLProperty(
//            ontology = OesoDataAnalysis.class,
//            property = "hasApplicationFile"
//    )
//    private URI applicationFileUri;
//    /**
//     * Rdf document date
//     */
//    @SPARQLProperty(
//            ontology = DCTerms.class,
//            property = "created"
//    )
//    private LocalDate applicationFileCreationDate;
//    
//      /**
//     * Rdf document date
//     */
//    @SPARQLProperty(
//            ontology = DCTerms.class,
//            property = "modified"
//    )
//    private LocalDate applicationFileModifiedDate;
    /**
     * Scientific application displayed name
     */
    @SPARQLProperty(
            ontology = RDFS.class,
            property = "label",
            required = true
    )
    private String name;
    public static String NAME_FIELD = "name";

    /**
     * Scientific application programming language
     */
    @SPARQLProperty(
            ontology = OesoDataAnalysis.class,
            property = "hasProgrammingLanguage",
            required = true
    )
    private String hasProgrammingLanguage;
    public static String HAS_PROGRAMMING_LANGUAGE_FIELD = "hasProgrammingLanguage";

    /**
     * Scientific application description
     */
    @SPARQLProperty(
            ontology = RDFS.class,
            property = "comment"
    )
    private String description;

    @SPARQLProperty(
        ontology = DCTerms.class,
        property = "source"
    )
    URI source;
    public static final String SOURCE_FIELD = "source";

    /**
     * Scientific application docker command line
     */
    private ArrayList<String> containerCmd;
    /**
     * Scientific application docker image
     */
    private String containerImageName;

    /**
     * If document as been successfully extracted to dicker apps dir
     */
    private Boolean extractDockerFilesState;

//    public URI getApplicationFileUri() {
//        return applicationFileUri;
//    }
//
//    public void setApplicationFileUri(URI applicationFileUri) {
//        this.applicationFileUri = applicationFileUri;
//    }

//    public LocalDate getApplicationFileCreationDate() {
//        return applicationFileCreationDate;
//    }
//
//    public void setApplicationFileCreationDate(LocalDate applicationFileCreationDate) {
//        this.applicationFileCreationDate = applicationFileCreationDate;
//    }
//
//    public LocalDate getApplicationFileModifiedDate() {
//        return applicationFileModifiedDate;
//    }
//
//    public void setApplicationFileModifiedDate(LocalDate applicationFileModifiedDate) {
//        this.applicationFileModifiedDate = applicationFileModifiedDate;
//    }

    
    public String getName() {
        return name;
    }

    public void setName(String label) {
        this.name = label;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ArrayList<String> getContainerCmd() {
        return containerCmd;
    }

    public void setContainerCmd(ArrayList<String> containerCmd) {
        this.containerCmd = containerCmd;
    }

    public String getContainerImageName() {
        return containerImageName;
    }

    public void setContainerImageName(String containerImageName) {
        this.containerImageName = containerImageName;
    }

    public Boolean getExtractDockerFilesState() {
        return extractDockerFilesState;
    }

    public void setExtractDockerFilesState(Boolean extractDockerFilesState) {
        this.extractDockerFilesState = extractDockerFilesState;
    }

    public URI getSource() {
        return source;
    }

    public void setSource(URI source) {
        this.source = source;
    }


    public String getHasProgrammingLanguage() {
        return hasProgrammingLanguage;
    }

    public void setHasProgrammingLanguage(String hasProgrammingLanguage) {
        this.hasProgrammingLanguage = hasProgrammingLanguage;
    }

    // Method to encode a string value using `UTF-8` encoding scheme
    private static String encodeValue(String value) {
        try {
            return URLEncoder.encode(value, StandardCharsets.UTF_8.toString());
        } catch (UnsupportedEncodingException ex) {
            throw new RuntimeException(ex.getCause());
        }
    }

    public String getId() {
        MessageDigest md;
        try {
            md = MessageDigest.getInstance("MD5");
            md.update(getUri().toString().getBytes());
            byte[] digest = md.digest();
            String idHash = DatatypeConverter
                    .printHexBinary(digest).toLowerCase();
            return idHash;
        } catch (NoSuchAlgorithmException ex) {
            LOGGER.error(ex.getMessage(), ex);
            return null;
        }
    }

//    @Override
//    private String toString() {
////        Gson gson = new Gson();
////        return gson.toJson(this); //To change body of generated methods, choose Tools | Templates.
//    }
    @Override
    public String[] getInstancePathSegments(ScientificAppModel instance) {
        return new String[]{
            instance.getName()
        };
    }

}
