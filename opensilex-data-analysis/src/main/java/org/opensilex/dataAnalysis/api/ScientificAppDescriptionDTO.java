/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.opensilex.dataAnalysis.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.net.URI;
import org.opensilex.dataAnalysis.dal.ScientificAppModel;
import org.opensilex.server.rest.validation.Required;
import org.opensilex.server.rest.validation.ValidURI;

/**
 *
 * @author charlero
 */
public class ScientificAppDescriptionDTO {

    @ValidURI
    private URI uri;
 
    /**
     * Rdf document date
     */
    private String applicationFileCreationDate;

    /**
     * Scientific application displayed name
     */
    @Required
    private String name;

    @Required
    private String programmingLanguage;

    /**
     * Scientific application description
     */
    private String description;
    
    /**
     * Scientific application description
     */
 
    private URI source; 

    public URI getUri() {
        return uri;
    }

    public void setUri(URI uri) {
        this.uri = uri;
    }
  
    public String getApplicationFileCreationDate() {
        return applicationFileCreationDate;
    }

    public void setApplicationFileCreationDate(String applicationFileCreationDate) {
        this.applicationFileCreationDate = applicationFileCreationDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public URI getSource() {
        return source;
    }

    public void setSource(URI source) {
        this.source = source;
    }

    public String getProgrammingLanguage() {
        return programmingLanguage;
    }

    public void setProgrammingLanguage(String programmingLanguage) {
        this.programmingLanguage = programmingLanguage;
    }

    public ScientificAppModel newModel() {
        ScientificAppModel model = new ScientificAppModel();
        model.setUri(getUri());
        model.setHasProgrammingLanguage(getProgrammingLanguage());
        model.setName(getName());
        model.setDescription(getDescription());
        model.setSource(getSource());
        return model;
    }
     
    
    /**
     * Method to unserialize ScientificAppCreationDTO.
     * Required because this data is received as @FormDataParam.
     * @param param
     * @return
     * @throws IOException 
     */
    public static ScientificAppDescriptionDTO fromString(String param) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(param, ScientificAppDescriptionDTO.class);
    }
    
}
