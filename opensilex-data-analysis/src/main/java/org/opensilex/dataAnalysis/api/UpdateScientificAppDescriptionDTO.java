 
package org.opensilex.dataAnalysis.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.net.URI;
import org.opensilex.dataAnalysis.dal.ScientificAppModel;
import org.opensilex.server.rest.validation.ValidURI;

/**
 *
 * @author charlero
 */
public class UpdateScientificAppDescriptionDTO {
 
    @ValidURI
    private URI uri;
 
    /**
     * Rdf document date
     */
    private String applicationFileCreationDate;

    /**
     * Scientific application displayed name
     */ 
    private String name;

    /**
     * Scientific application description
     */
    private String description;

    public URI getUri() {
        return uri;
    }

    public void setUri(URI uri) {
        this.uri = uri;
    }
  
    public String getApplicationFileCreationDate() {
        return applicationFileCreationDate;
    }

    public void setApplicationFileCreationDate(String applicationFileCreationDate) {
        this.applicationFileCreationDate = applicationFileCreationDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ScientificAppModel newModel() {
        ScientificAppModel model = new ScientificAppModel();
        model.setUri(getUri());
        model.setName(getName());
        model.setDescription(getDescription()); 
        return model;
    }
     
    
    /**
     * Method to unserialize ScientificAppCreationDTO.
     * Required because this data is received as @FormDataParam.
     * @param param
     * @return
     * @throws IOException 
     */
    public static ScientificAppDescriptionDTO fromString(String param) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(param, ScientificAppDescriptionDTO.class);
    }
    
}
