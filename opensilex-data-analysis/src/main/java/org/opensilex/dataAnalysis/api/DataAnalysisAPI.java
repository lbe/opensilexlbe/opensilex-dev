//******************************************************************************
//                          DataAnalysisAPI.java
// OpenSILEX - Licence AGPL V3.0 - https://www.gnu.org/licenses/agpl-3.0.en.html
// Copyright © INRAE 2020
// Contact: arnaud.charleroy@inrae.fr, anne.tireau@inrae.fr, pascal.neveu@inrae.fr
//******************************************************************************
package org.opensilex.dataAnalysis.api;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.annotations.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.net.URI;
import java.nio.file.NoSuchFileException;
import java.text.NumberFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.inject.Inject;
import javax.validation.ConstraintViolation;
import javax.validation.Valid;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import org.apache.commons.io.FileUtils;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;
import org.opensilex.dataAnalysis.dal.ScientificAppDAO;
import org.opensilex.dataAnalysis.dal.ScientificAppModel;
import org.opensilex.dataAnalysis.opencpu.OpencpuServer;
import org.opensilex.dataAnalysis.server.response.InformationDTO;
import org.opensilex.dataAnalysis.server.response.InformationResponse;
import org.opensilex.dataAnalysis.shinyProxy.ShinyProxyConfig;
import org.opensilex.dataAnalysis.shinyProxy.ShinyProxyServer;
import org.opensilex.dataAnalysis.shinyProxy.ShinyProxyStatus;
import org.opensilex.fs.service.FileStorageService;
import org.opensilex.security.account.dal.AccountModel;
import org.opensilex.security.authentication.ApiCredential;
import org.opensilex.security.authentication.ApiCredentialGroup;
import org.opensilex.security.authentication.ApiProtected;
import org.opensilex.security.authentication.injection.CurrentUser;
import org.opensilex.server.response.ErrorDTO;
import org.opensilex.server.response.ErrorResponse;
import org.opensilex.server.response.ObjectUriResponse;
import org.opensilex.server.response.PaginatedListResponse;
import org.opensilex.server.response.SingleObjectResponse;
import org.opensilex.sparql.exceptions.SPARQLAlreadyExistingUriException;
import org.opensilex.sparql.exceptions.SPARQLInvalidURIException;
import org.opensilex.sparql.service.SPARQLService;
import org.opensilex.utils.ListWithPagination;
import org.opensilex.utils.OrderBy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Arnaud Charleroy
 */
@Api(DataAnalysisAPI.CREDENTIAL_DATA_ANALYSIS_GROUP_ID)
@Path("/data-analysis")
@ApiCredentialGroup(
        groupId = DataAnalysisAPI.CREDENTIAL_DATA_ANALYSIS_GROUP_ID,
        groupLabelKey = DataAnalysisAPI.CREDENTIAL_DATA_ANALYSIS_GROUP_LABEL_KEY
)
public class DataAnalysisAPI {

    Logger LOGGER = LoggerFactory.getLogger(DataAnalysisAPI.class);

    @Inject
    OpencpuServer opencpu;

    @Inject
    ShinyProxyServer shinyProxy;

    @Inject
    SPARQLService sparql;

    @Inject
    FileStorageService fs;

    @CurrentUser
    AccountModel currentUser;

    public static final String CREDENTIAL_DATA_ANALYSIS_GROUP_ID = "DataAnalysis";
    public static final String CREDENTIAL_DATA_ANALYSIS_GROUP_LABEL_KEY = "credential-groups.dataAnalysis";

    public static final String CREDENTIAL_DATA_ANALYSIS_MODIFICATION_ID = "dataAnalysis-modification";
    public static final String CREDENTIAL_DATA_ANALYSIS_MODIFICATION_LABEL_KEY = "credential.dataAnalysis.modification";

    public static final String CREDENTIAL_DATA_ANALYSIS_DELETE_ID = "dataAnalysis-delete";
    public static final String CREDENTIAL_DATA_ANALYSIS_DELETE_LABEL_KEY = "credential.dataAnalysis.delete";

    public static final String SCIENTIFIC_APP_URI_EXAMPLE = "dev-ScientificApplication:test-regression";

    /**
     * Create a scientificApp model from a FactorCreationDTO object
     *
     * @param dto description
     * @param inFile file
     * @param fileDetail
     * @return ScientificApp URI
     * @throws Exception if creation failed
     */
    @POST
    @Path("scientific-app/create")
    @ApiOperation("Create an scientificApp")
    @ApiProtected
    @ApiCredential(
            credentialId = CREDENTIAL_DATA_ANALYSIS_MODIFICATION_ID,
            credentialLabelKey = CREDENTIAL_DATA_ANALYSIS_MODIFICATION_LABEL_KEY
    )
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Response createScientificApp(
        @ApiParam(value = "Application description with metadata", required = true, type = "string") @NotNull @Valid @FormDataParam("description") ScientificAppDescriptionDTO dto,
        @ApiParam(value = "Data file", required = true, type = "file") @FormDataParam("file") InputStream inFile,
        @FormDataParam("file") FormDataContentDisposition fileDetail
    ) throws Exception {

        // Test dto
        Set<ConstraintViolation<ScientificAppDescriptionDTO>> validateFileJsonCreationDTO = validateFileJsonCreationDTO(dto);
        if (!validateFileJsonCreationDTO.isEmpty()) {
            return new ErrorResponse(
                    Response.Status.BAD_REQUEST,
                    "Bad file metadata format",
                    validateFileJsonCreationDTO.toString()
            ).getResponse();
        }

        // Test  File Size
        if (fileDetail.getSize() == 0 && dto.getSource() == null) {
            return new ErrorResponse(
                    Response.Status.BAD_REQUEST,
                    "Bad file or missing source",
                    "Empty file size equals to 0"
            ).getResponse();
        }
 
        ScientificAppDAO dao = new ScientificAppDAO(sparql, fs);
        try {
            ScientificAppModel model = dto.newModel();
            model.setPublisher(currentUser.getUri());
            File file = null;
            if (fileDetail.getSize() != 0) {
                file = File.createTempFile(model.getName(), ".zip");
                FileUtils.copyInputStreamToFile(inFile, file);
            }

            dao.create(model, file);
            return new ObjectUriResponse(Response.Status.CREATED, model.getUri()).getResponse();
        } catch (SPARQLAlreadyExistingUriException duplicateUriException) {
            return new ErrorResponse(
                    Response.Status.CONFLICT,
                    "scientificApp already exists",
                    duplicateUriException.getMessage()
            ).getResponse();
        }

    }

    /**
     * Retreive application by uri
     *
     * @param uri application uri
     * @return Return application detail
     * @throws Exception in case of server error
     */
    @GET
    @Path("get/{uri}/download")
    @ApiOperation("Get an application")
    @ApiProtected
    @ApiCredential(credentialId = CREDENTIAL_DATA_ANALYSIS_MODIFICATION_ID, credentialLabelKey = CREDENTIAL_DATA_ANALYSIS_MODIFICATION_LABEL_KEY)
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    @ApiResponses(value = {
        @ApiResponse(code = 404, message = "File not found", response = ErrorResponse.class)})
    public Response downloadApplication(
            @ApiParam(value = "Application URI", example = "platform-application:detection-outlier", required = true) @PathParam("uri") @NotNull URI uri)
            throws Exception {
        ScientificAppDAO dao = new ScientificAppDAO(sparql, fs);
        ScientificAppModel model = dao.get(uri);

        if (model != null) {
            Response response;
            NumberFormat myFormat = NumberFormat.getInstance();
            try {
                byte[] file = dao.getFile(uri);
                ResponseBuilder builder = Response.ok(file);
                builder.header("Content-Disposition", "attachment; filename=" + model.getName());

                response = builder.build();

                long file_size = file.length;
                LOGGER.debug(String.format("Inside downloadFile==> fileName: %s, fileSize: %s bytes",
                        model.getName(), myFormat.format(file_size)));
            } catch (NoSuchFileException | FileNotFoundException ex) {
                LOGGER.error(String.format("Inside downloadFile==> FILE NOT FOUND: fileName: %s",
                        model.getName()));

                response = Response.status(404).
                        entity("FILE NOT FOUND: " + uri).
                        type("text/plain").
                        build();

            }

            return response;
        } else {
            return new ErrorResponse(Response.Status.NOT_FOUND, "Application not found",
                    "Unknown application URI: " + uri.toString()).getResponse();
        }

    }

    /**
     * Retreive application by uri
     *
     * @param uri application uri
     * @return Return application detail
     * @throws Exception in case of server error
     */
    @GET
    @Path("get/{uri}")
    @ApiOperation("Get an application")
    @ApiProtected
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Application retrieved", response = ScientificAppGetDTO.class),
        @ApiResponse(code = 404, message = "Application not found", response = ErrorResponse.class)})
    public Response getApplication(
            @ApiParam(value = "Application URI", example = "platform-application:detection-outlier", required = true) @PathParam("uri") @NotNull URI uri)
            throws Exception {
        ScientificAppDAO dao = new ScientificAppDAO(sparql, fs);
        ScientificAppModel model = dao.get(uri);
        ShinyProxyConfig config = (ShinyProxyConfig) shinyProxy.getConfig();

        if (model != null) {
            return new SingleObjectResponse<>(ScientificAppGetDTO.fromModel(model, config.landingPage(), shinyProxy.serverUrl(),null)).getResponse();
        } else {
            return new ErrorResponse(Response.Status.NOT_FOUND, "Application not found",
                    "Unknown application URI: " + uri.toString()).getResponse();
        }
    }

    /**
     * Call R function via OpenCPU Server
     *
     * @param packageName R package name
     * @param functionName function Name
     * @param jsonParameters function parameters in Json
     * @return Response
     * @throws com.fasterxml.jackson.core.JsonProcessingException
     * @example
     */
    @POST
    @Path("r-call")
    @ApiOperation(value = "Get a json value",
            notes = "Retrieve numerical array result response from a R call")
    @ApiResponses(value = {
        @ApiResponse(code = 201, message = "Retrieve data from R call"),
        @ApiResponse(code = 400, message = "Bad user information")
    })
    @ApiProtected
    @Produces(MediaType.APPLICATION_JSON)
    public Response getRNumericalFunctionResults(
            @ApiParam(required = true, value = "R package name") @QueryParam("packageName") @DefaultValue("stats") @NotEmpty String packageName,
            @ApiParam(required = true, value = "R function name") @QueryParam("functionName") @DefaultValue("rnorm") @NotEmpty String functionName,
            @ApiParam(value = "R function paramaters") @QueryParam("jsonParameters") @DefaultValue("{\"n\":200}") String jsonParameters) throws JsonProcessingException {
        if (!opencpu.getConfig().enable()) {
            return new InformationResponse(
                    Response.Status.SERVICE_UNAVAILABLE,
                    "R server status",
                    "R server not enabled "
            ).getResponse();
        }
        try (Response functionCallResponse = opencpu.opencpuRFunctionProxyCall(packageName, functionName, jsonParameters)) {
            String stringResult = functionCallResponse.readEntity(String.class);

            if (functionCallResponse.getStatus() != Response.Status.CREATED.getStatusCode()) {
                return new ErrorResponse(
                        Response.Status.BAD_REQUEST,
                        "Request error",
                        stringResult
                ).getResponse();
            } else {
                ObjectMapper mapper = new ObjectMapper();
                Number[] array = mapper.readValue(stringResult, Number[].class);
                return new SingleObjectResponse<>(
                        array
                ).getResponse();
            }
        }
    }

    /**
     * Search scientificApps
     *
     * @param name Scientifc app name
     * @param orderByList order by
     * @see org.opensilex.core.scientificApp.dal.FactorDAO
     * @param page Page number
     * @param pageSize Page size
     * @return filtered, ordered and paginated list
     * @throws Exception Return a 500 - INTERNAL_SERVER_ERROR error response
     */
    @POST
    @Path("scientific-app/search")
    @ApiOperation("Search scientificApps")
    @ApiProtected
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Return scientificApp list", response = ScientificAppGetDTO.class, responseContainer = "List"),
        @ApiResponse(code = 400, message = "Invalid parameters", response = ErrorDTO.class)
    })
    public Response searchScientificApp(
            @ApiParam(value = "scientific App name", example = "search outliers") @QueryParam("name") String name,
            @ApiParam(value = "scientific App name", example = "search outliers") @QueryParam("programming_language") String programmingLanguage,
            @ApiParam(value = "List of fields to sort as an array of fieldName=asc|desc", example = "name=asc") @QueryParam("orderBy") List<OrderBy> orderByList,
            @ApiParam(value = "Page number", example = "0") @QueryParam("page") @DefaultValue("0") @Min(0) int page,
            @ApiParam(value = "Page size", example = "20") @QueryParam("pageSize") @DefaultValue("20") @Min(0) int pageSize
    ) throws Exception {

        // Search scientificApps with ScientificApp DAO
        ScientificAppDAO dao = new ScientificAppDAO(sparql, fs);
        ListWithPagination<ScientificAppModel> resultList = dao.search(
                name,
                programmingLanguage,
                orderByList,
                page,
                pageSize
        );

        List<ScientificAppGetDTO> resultDTOList = new ArrayList<>();
        ShinyProxyConfig config = (ShinyProxyConfig) shinyProxy.getConfig();
        resultList.getList().forEach((scientificAppModel) -> {
            resultDTOList.add(ScientificAppGetDTO.fromModel(scientificAppModel, config.landingPage(), shinyProxy.serverUrl(), currentUser.getToken()));
        });
        ListWithPagination<ScientificAppGetDTO> paginatedListResponse = new ListWithPagination<>(
                resultDTOList, resultList.getPage(),
                resultList.getPageSize(),
                resultList.getTotal()
        );

        // Return paginated list of scientificApp DTO
        return new PaginatedListResponse<>(paginatedListResponse).getResponse();
    }

    @POST
    @Path("shiny-server-reload")
    @ApiOperation(value = "Get a server status message")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Server Reloading", response = InformationDTO.class),
        @ApiResponse(code = 503, message = "Not running", response = ErrorDTO.class)

    })
    @ApiProtected
    @ApiCredential(
            credentialId = CREDENTIAL_DATA_ANALYSIS_MODIFICATION_ID,
            credentialLabelKey = CREDENTIAL_DATA_ANALYSIS_MODIFICATION_LABEL_KEY
    )
    @Produces(MediaType.APPLICATION_JSON)
    public Response shinyServerReload() throws Exception {
        ShinyProxyStatus status = ShinyProxyServer.status;

        if (status.equals(ShinyProxyStatus.ONLINE)) {
            shinyProxy.reload(sparql,fs);
        }

        return new InformationResponse(
                Response.Status.OK,
                "RELOAD",
                "Reloading apps"
        ).getResponse();

    }

    /**
     * Remove an application
     *
     * @param scientificAppUri the scientificApp URI
     * @return a {@link Response} with a {@link ObjectUriResponse} containing
     * the deleted ScientificApp {@link URI}
     */
    @DELETE
    @Path("scientific-app/delete/{uri}")
    @ApiOperation("Delete an scientific app")
    @ApiProtected
    @ApiCredential(
            credentialId = CREDENTIAL_DATA_ANALYSIS_DELETE_ID,
            credentialLabelKey = CREDENTIAL_DATA_ANALYSIS_DELETE_LABEL_KEY
    )
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "ScientificApp deleted", response = ObjectUriResponse.class),
        @ApiResponse(code = 400, message = "Invalid or unknown ScientificApp URI", response = ErrorResponse.class),
        @ApiResponse(code = 500, message = "Internal Server Error", response = ErrorResponse.class)})
    public Response deleteScientificApp(
            @ApiParam(value = "ScientificApp URI", example = SCIENTIFIC_APP_URI_EXAMPLE, required = true) @PathParam("uri") @NotNull URI scientificAppUri
    ) {
        try {
            ScientificAppDAO dao = new ScientificAppDAO(sparql, fs);
            dao.delete(scientificAppUri);
            return new ObjectUriResponse(scientificAppUri).getResponse();

        } catch (SPARQLInvalidURIException e) {
            return new ErrorResponse(Response.Status.BAD_REQUEST, "Invalid or unknown ScientificApp URI", e.getMessage()).getResponse();
        } catch (Exception e) {
            return new ErrorResponse(e).getResponse();
        }
    }

    @POST
    @Path("shiny-server-status")
    @ApiOperation(value = "Get a server status message")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Server OK", response = InformationDTO.class),
        @ApiResponse(code = 503, message = "Not running", response = ErrorDTO.class)

    })
    @Produces(MediaType.APPLICATION_JSON)
    public Response shinyServerStatus() throws Exception {
        Response.Status responseCode;
        String message = null;
        ShinyProxyStatus status = ShinyProxyServer.status;

        switch (status) {
            case OFFLINE:
                responseCode = Response.Status.SERVICE_UNAVAILABLE;
                message = "Server is offline";
                break;
            case ONLINE:
                responseCode = Response.Status.OK;
                message = "Server is online";
                break;
            case UPDATING:
                responseCode = Response.Status.OK;
                message = "Server is updating app";
                break;
            case STARTING:
                responseCode = Response.Status.SERVICE_UNAVAILABLE;
                message = "Server is not available";
                break;
            default:
                responseCode = Response.Status.OK;
        }

        return new InformationResponse(
                responseCode,
                status.toString(),
                message
        ).getResponse();

    }

    private Set<ConstraintViolation<ScientificAppDescriptionDTO>> validateFileJsonCreationDTO(ScientificAppDescriptionDTO dto) {
        Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
        Set<ConstraintViolation<ScientificAppDescriptionDTO>> validate = validator.validate(dto);
        return validate;
    }
}
