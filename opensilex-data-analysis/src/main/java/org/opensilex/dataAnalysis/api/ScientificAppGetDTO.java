/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.opensilex.dataAnalysis.api;

import java.net.URI;
import java.net.URL;
import java.time.LocalDate;
import java.time.OffsetDateTime;
import org.opensilex.dataAnalysis.dal.ScientificAppModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author charlero
 */
class ScientificAppGetDTO {
    final static Logger LOGGER = LoggerFactory.getLogger(ScientificAppGetDTO.class);

    private URI uri;

    /**
     * Rdf document date
     */
    private String applicationUrl;

    /**
     * Scientific application displayed name
     */
    private String name;

    /**
     * Scientific application displayed name
     */
    private String programmingLanguage;
    /**
     * Scientific application description
     */
    private String description;
    
    private String author;

    private OffsetDateTime creationDate;
            
    private OffsetDateTime modifiedDate;
    
    private URI source;

    public URI getUri() {
        return uri;
    }

    public void setUri(URI uri) {
        this.uri = uri;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getApplicationUrl() {
        return applicationUrl;
    }

    public void setApplicationUrl(String applicationUrl) {
        this.applicationUrl = applicationUrl;
    }

    private void createApplicationUrl(URL serverUrl, String landingPage, String token, String id) {
        if (token == null) {
            this.setApplicationUrl(serverUrl + landingPage + "app/" + id);
        } else {
            this.setApplicationUrl(serverUrl + landingPage + "app/" + id + "?token=" + token);
        }
    }

    public OffsetDateTime getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(OffsetDateTime creationDate) {
        this.creationDate = creationDate;
    }

    public OffsetDateTime getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(OffsetDateTime modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public URI getSource() {
        return source;
    }

    public void setSource(URI source) {
        this.source = source;
    }

    public String getProgrammingLanguage() {
        return programmingLanguage;
    }

    public void setProgrammingLanguage(String programmingLanguage) {
        this.programmingLanguage = programmingLanguage;
    }

    public static ScientificAppGetDTO fromModel(ScientificAppModel model, String landingPage, URL serverUrl, String token) {
        ScientificAppGetDTO dto = new ScientificAppGetDTO();
        dto.setUri(model.getUri());
        dto.setName(model.getName());
        dto.setProgrammingLanguage(model.getHasProgrammingLanguage());
        dto.setDescription(model.getDescription());
        dto.setCreationDate(model.getPublicationDate());
        dto.setModifiedDate(model.getLastUpdateDate());
        dto.setSource(model.getSource());
        dto.createApplicationUrl(serverUrl, landingPage, token, model.getId());
        return dto;
    }
}

     
