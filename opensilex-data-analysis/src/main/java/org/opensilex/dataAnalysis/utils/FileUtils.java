/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.opensilex.dataAnalysis.utils;
 
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author charlero
 */
public class FileUtils {
    
    final static Logger LOGGER  = LoggerFactory.getLogger(FileUtils.class);

    public static void copyResourcefileToDir(InputStream resourceAsStream, Path dockerFileFilePath) throws IOException {
        Files.copy(
                resourceAsStream,
                dockerFileFilePath,
                StandardCopyOption.REPLACE_EXISTING
        );
    }
    
    public static void copyResourceDirToDir(Path srcDir, Path destDir) throws IOException {
        org.apache.commons.io.FileUtils.deleteDirectory(destDir.toFile());
        org.apache.commons.io.FileUtils.copyDirectory(srcDir.toFile(), destDir.toFile()); 
    }
    
    /**
     * Unzip an zip archive
     *
     * @param zipFile
     * @param extractFolder
     * @return
     */
    public static boolean unzipFile(String zipFile, String extractFolder) {
        try {
            int BUFFER = 2048;
            File file = new File(zipFile);

            ZipFile zip = new ZipFile(file);
            String newPath = extractFolder;

            new File(newPath).mkdir();
            Enumeration zipFileEntries = zip.entries();

            // Process each entry
            while (zipFileEntries.hasMoreElements()) {
                // grab a zip file entry
                ZipEntry entry = (ZipEntry) zipFileEntries.nextElement();
                String currentEntry = entry.getName();

                File destFile = new File(newPath, currentEntry);
                //destFile = new File(newPath, destFile.getName());
                File destinationParent = destFile.getParentFile();

                // create the parent directory structure if needed
                destinationParent.mkdirs();

                if (!entry.isDirectory()) {
                    BufferedInputStream is = new BufferedInputStream(zip
                            .getInputStream(entry));
                    int currentByte;
                    // establish buffer for writing file
                    byte data[] = new byte[BUFFER];

                    // write the current file to disk
                    FileOutputStream fos = new FileOutputStream(destFile);
                    BufferedOutputStream dest = new BufferedOutputStream(fos,
                            BUFFER);

                    // read and write until last byte is encountered
                    while ((currentByte = is.read(data, 0, BUFFER)) != -1) {
                        dest.write(data, 0, currentByte);
                    }
                    dest.flush();
                    dest.close();
                    is.close();

                }

            }
        } catch (IOException ex) {
            LOGGER.error(ex.getMessage(), ex);
            return false;
        }
        return true;
    }
}
