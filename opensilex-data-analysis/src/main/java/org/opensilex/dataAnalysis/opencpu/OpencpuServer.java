package org.opensilex.dataAnalysis.opencpu;

//******************************************************************************
// OpenSILEX - Licence AGPL V3.0 - https://www.gnu.org/licenses/agpl-3.0.en.html
// Copyright © INRA 2019
// Contact: arnaud.charleroy@inrae.fr, anne.tireau@inrae.fr, pascal.neveu@inrae.fr
//******************************************************************************
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.opensilex.dataAnalysis.utils.DockerUtils;
import org.opensilex.dataAnalysis.exceptions.DockerException;
import org.opensilex.server.extensions.ServerExtension;
import org.opensilex.service.BaseService;
import org.opensilex.service.ServiceDefaultDefinition;

/**
 * Implementation of ShinyProxyService
 */
@ServiceDefaultDefinition( config = 
        OpenCPUConfig.class
)
public class OpencpuServer extends BaseService implements ServerExtension {
    
    public final static String DEFAULT_OPENCPU_SERVICE = "opencpu";

    private final static Logger LOGGER = LoggerFactory.getLogger(OpencpuServer.class);

    private static String OPENCPU_CONTAINER_NAME;

    private final OpenCPUConfig config;

    public OpencpuServer(OpenCPUConfig openCPUConfig) {
        super(openCPUConfig);
        config = openCPUConfig;
        OPENCPU_CONTAINER_NAME = "opensilex-ocpu-" + openCPUConfig.dockerImageTag();
    }

    public OpenCPUConfig getConfig() {
        return config;
    }

    @Override
    public void startup() throws Exception {
        LOGGER.debug("Launch server OpenCPU server on port :" + config.port().toString());
        if (config.enable()) {
            // If Docker service is running
            if (!DockerUtils.checkDockerIsRunning()) {
                throw new DockerException("Docker is not running");
            }

            // check if Docker image exist
            if (!DockerUtils.checkDockerImageExist(config.dockerImage(), config.dockerImageTag())) {
                LOGGER.debug(config.dockerImage() + " not exist and will be downloaded");
            }

            // Run container
            if (DockerUtils.checkDockerContainerExist(OPENCPU_CONTAINER_NAME)) {
                LOGGER.warn("Stop and remove container" + OPENCPU_CONTAINER_NAME);
                DockerUtils.dockerStopRemoveContainer(OPENCPU_CONTAINER_NAME);
            }

            // Run container 
            List<String> dockerRunArgs = Stream.of("-d",
                    "-v",
                    "/var/run/docker.sock:/var/run/docker.sock",
                    "--name",
                    OPENCPU_CONTAINER_NAME,
                    "-p",
                    config.port() + ":8004",
                    config.dockerImage() + ":" + config.dockerImageTag())
                    .collect(Collectors.toList());

            if (!DockerUtils.dockerRun(dockerRunArgs)) {
                throw new DockerException("Container " + OPENCPU_CONTAINER_NAME + " failed to start");
            }

        }
    }

    @Override
    public void shutdown() throws Exception {
        if (config.enable()) {
            DockerUtils.stopDockerContainer(OPENCPU_CONTAINER_NAME);
        }
    }

    public Response opencpuRFunctionProxyCall(String packageName, String functionName, String parameters) {
        Client client = ClientBuilder.newClient();
        WebTarget opencpuCallWebTarget;
        LOGGER.debug(config.httpScheme() + "://" + config.host() + ":" + config.port() + "/ocpu/library");

        opencpuCallWebTarget = client.target(config.httpScheme() + "://" + config.host() + ":" + config.port() + "/ocpu/library")
                .path("{packageName}/R/{functionName}/json")
                .resolveTemplate("packageName", packageName)
                .resolveTemplate("functionName", functionName)
                .queryParam("auto_unbox", "TRUE");
        LOGGER.debug(opencpuCallWebTarget.getUri().toString() + " with parameters " + Entity.json(parameters));
        return opencpuCallWebTarget.request(MediaType.APPLICATION_JSON).post(Entity.json(parameters));
    }

}
