/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.opensilex.dataAnalysis.opencpu;

import org.opensilex.config.ConfigDescription;
import org.opensilex.service.ServiceConfig;

/**
 *
 * @author Arnaud Charleroy
 */
public interface OpenCPUConfig extends ServiceConfig {

    @ConfigDescription(
            value = "Activate or deactivate shiny proxy server",
            defaultBoolean = false
    )
    Boolean enable();

    @ConfigDescription(
            value = "Http scheme parameter",
            defaultString = "http"
    )
    String httpScheme();

    @ConfigDescription(
            value = "Host parameter",
            defaultString = "localhost"
    )
    String host();

    @ConfigDescription(
            value = "Port parameter",
            defaultInt = 8098
    )
    Integer port();

    @ConfigDescription(
            value = "Docker image",
            defaultString = "opencpu/rstudio"
    )
    String dockerImage();

    @ConfigDescription(
            value = "Docker image tag",
            defaultString = "2.1.6"
    )
    String dockerImageTag();
}
