/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.opensilex.dataAnalysis.service;

import org.opensilex.dataAnalysis.exceptions.ShinyProxyException;
import org.opensilex.dataAnalysis.shinyProxy.ShinyProxyConfig;
import org.opensilex.dataAnalysis.shinyProxy.ShinyProxyServer;
import org.opensilex.service.BaseService;
import org.opensilex.service.Service;
import org.opensilex.service.ServiceConfig;
import org.opensilex.service.ServiceDefaultDefinition;

@ServiceDefaultDefinition(implementation = ShinyProxyServer.class)
public class ScientificAppService extends BaseService implements ScientificAppServer {

    public ScientificAppService(ScientificAppServer scientificApplicationServer, ServiceConfig config) {
        super(config);
        this.scientificApplicationServer = scientificApplicationServer;
    } 
 
    private final ScientificAppServer scientificApplicationServer;
    
    @Override
    public void setup() throws Exception {
        scientificApplicationServer.setOpenSilex(getOpenSilex());
        scientificApplicationServer.setup();
    }

    @Override
    public void startup() throws Exception {
        scientificApplicationServer.startup();
    }

    @Override
    public void shutdown() throws Exception {
        scientificApplicationServer.shutdown();
    }
 
    @Override
    public void serverUrl() throws ShinyProxyException {
        scientificApplicationServer.serverUrl();
    }
    
}
