/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.opensilex.dataAnalysis.service;

import org.opensilex.dataAnalysis.exceptions.ShinyProxyException;
import org.opensilex.service.Service;

/**
 *
 * @author charlero
 */
public interface ScientificAppServer  extends Service { 
  
     /**
     *
     * @throws ShinyProxyException
     */
    public void serverUrl() throws ShinyProxyException;

}
