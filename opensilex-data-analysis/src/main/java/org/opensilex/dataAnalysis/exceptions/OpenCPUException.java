//******************************************************************************
// OpenSILEX - Licence AGPL V3.0 - https://www.gnu.org/licenses/agpl-3.0.en.html
// Copyright © INRA 2019
// Contact: arnaud.charleroy@inrae.fr, anne.tireau@inrae.fr, pascal.neveu@inrae.fr
//******************************************************************************
package org.opensilex.dataAnalysis.exceptions;

/**
 *
 * @author vincent
 */
public class OpenCPUException extends Exception {

    public OpenCPUException() {
        super();
    }

    public OpenCPUException(String message) {
        super(message);
    }

    public OpenCPUException(String message, Throwable cause) {
        super(message, cause);
    }

    public OpenCPUException(Throwable cause) {
        super(cause);
    }

}
