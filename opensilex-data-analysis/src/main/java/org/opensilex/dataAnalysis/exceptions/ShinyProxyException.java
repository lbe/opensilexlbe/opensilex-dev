//******************************************************************************
// OpenSILEX - Licence AGPL V3.0 - https://www.gnu.org/licenses/agpl-3.0.en.html
// Copyright © INRA 2019
// Contact: arnaud.charleroy@inrae.fr, anne.tireau@inrae.fr, pascal.neveu@inrae.fr
//******************************************************************************
package org.opensilex.dataAnalysis.exceptions;

/**
 *
 * @author vincent
 */
public class ShinyProxyException extends Exception {

    public ShinyProxyException() {
        super();
    }

    public ShinyProxyException(String message) {
        super(message);
    }

    public ShinyProxyException(String message, Throwable cause) {
        super(message, cause);
    }

    public ShinyProxyException(Throwable cause) {
        super(cause);
    }

}
