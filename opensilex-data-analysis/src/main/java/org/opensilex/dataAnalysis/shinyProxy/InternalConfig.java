/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.opensilex.dataAnalysis.shinyProxy;

import org.opensilex.config.ConfigDescription;

/**
 * Internal config
 * @author Arnaud Charleroy
 */
public interface InternalConfig {
    @ConfigDescription(
               value = "title",
               defaultString = "OpenSILEX Shiny Proxy"
    )
    String title();
    
    @ConfigDescription(
            value = "title"
    )
    String logoUrl();    
    
    @ConfigDescription(
            value = "title",
            defaultBoolean = true
    )
    Boolean hideNavbar(); 
    
    @ConfigDescription(
            value = "Authentication parameter",
            defaultString = "none"
    )
    String authentication();

    @ConfigDescription(
            value = "Container waiting time",
            defaultInt = 60000
    )
    Integer containerWaitTime();

//  specs:
//  - id: 01_hello
//    display-name: Hello Application
//    description: Application which demonstrates the basics of a Shiny app
//    container-cmd: ["R", "-e", "shinyproxy::run_01_hello()"]
//    container-image: openanalytics/shinyproxy-demo
//    container-network: shiny-proxy-network
//logging:
//  file:
//    shinyproxy.log

}
