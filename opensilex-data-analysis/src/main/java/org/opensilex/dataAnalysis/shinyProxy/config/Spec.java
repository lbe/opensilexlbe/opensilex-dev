/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.opensilex.dataAnalysis.shinyProxy.config;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "id",
    "display-name",
    "description",
    "container-cmd",
    "container-image",
    "container-network"
})
public class Spec {

    @JsonProperty("id")
    private String id;
    @JsonProperty("display-name")
    private String displayName;
    @JsonProperty("description")
    private String description;
    @JsonProperty("container-cmd")
    private List<String> containerCmd = null;
    @JsonProperty("container-image")
    private String containerImage;
    @JsonProperty("container-network")
    private String containerNetwork;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("id")
    public String getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    @JsonProperty("display-name")
    public String getDisplayName() {
        return displayName;
    }

    @JsonProperty("display-name")
    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    @JsonProperty("container-cmd")
    public List<String> getContainerCmd() {
        return containerCmd;
    }

    @JsonProperty("container-cmd")
    public void setContainerCmd(List<String> containerCmd) {
        this.containerCmd = containerCmd;
    }

    @JsonProperty("container-image")
    public String getContainerImage() {
        return containerImage;
    }

    @JsonProperty("container-image")
    public void setContainerImage(String containerImage) {
        this.containerImage = containerImage;
    }

    @JsonProperty("container-network")
    public String getContainerNetwork() {
        return containerNetwork;
    }

    @JsonProperty("container-network")
    public void setContainerNetwork(String containerNetwork) {
        this.containerNetwork = containerNetwork;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
