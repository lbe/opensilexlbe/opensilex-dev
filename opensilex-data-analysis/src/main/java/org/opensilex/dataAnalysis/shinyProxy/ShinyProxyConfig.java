/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.opensilex.dataAnalysis.shinyProxy;

import org.opensilex.config.ConfigDescription;
import org.opensilex.service.ServiceConfig;

/**
 *
 * @author Arnaud Charleroy
 */
public interface ShinyProxyConfig extends ServiceConfig {

    @ConfigDescription(
            value = "Activate or deactivate shiny proxy server",
            defaultBoolean = true
    )
    Boolean enable();

    @ConfigDescription(
            value = "Public server URL parameter",
            defaultString = ""
    )
    String publicServerURL();

    @ConfigDescription(
            value = "Http scheme parameter",
            defaultString = "http"
    )
    String httpScheme();

    @ConfigDescription(
            value = "Host parameter",
            defaultString = "localhost"
    )
    String host();

    @ConfigDescription(
            value = "Port parameter",
            defaultInt = 8097
    )
    Integer port();

    @ConfigDescription(
            value = "landing page parameter",
            defaultString = "/"
    )
    String landingPage();

    @ConfigDescription(
            value = "Specific existing config file path"
    )
    String configFilePath();

    @ConfigDescription(
            value = "Docker network name",
            defaultString = "os-project-shiny-proxy-network"
    )
    String shinyProxyNetwork();

    @ConfigDescription(
            value = "Docker container prefix name (multiple shinyproxy on same server)",
            defaultString = "os-project"
    )
    String shinyProxyContainerPrefix();

    @ConfigDescription(
            value = "Proxy config"
    )
    InternalConfig internalDockerConfig();

    @ConfigDescription(
            value = "server.servlet.context-path",
            defaultString = "/"
    )
    String serverServletContextPath();

}
