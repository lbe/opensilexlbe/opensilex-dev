package org.opensilex.dataAnalysis.shinyProxy;

//**************************************************************************
import com.apicatalog.jsonld.StringUtils;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.opensilex.dataAnalysis.dal.ScientificAppDAO;
import org.opensilex.dataAnalysis.dal.ScientificAppModel;
import org.opensilex.dataAnalysis.utils.DockerUtils;
import org.opensilex.dataAnalysis.exceptions.DockerException;
import org.opensilex.dataAnalysis.exceptions.ShinyProxyException;
import org.opensilex.dataAnalysis.shinyProxy.config.Docker;
import org.opensilex.dataAnalysis.shinyProxy.config.Proxy;
import org.opensilex.dataAnalysis.shinyProxy.config.Server;
import org.opensilex.dataAnalysis.shinyProxy.config.Servlet;
import org.opensilex.dataAnalysis.shinyProxy.config.ShinyProxyConfigYaml;
import org.opensilex.dataAnalysis.shinyProxy.config.Spec;
import org.opensilex.dataAnalysis.utils.FileUtils;
import org.opensilex.fs.service.FileStorageService;
import org.opensilex.service.BaseService;
import org.opensilex.service.ServiceDefaultDefinition;
import org.opensilex.sparql.deserializer.SPARQLDeserializers;
import org.opensilex.sparql.service.SPARQLService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

///**
// * Implementation of ShinyProxyServer
// */
@ServiceDefaultDefinition(config
        = ShinyProxyConfig.class
)
public class ShinyProxyServer extends BaseService {

    final Logger LOGGER = LoggerFactory.getLogger(ShinyProxyServer.class);

    public final static String DEFAULT_SHINY_PROXY_SERVICE = "shinyProxy";

    private final ShinyProxyConfig config;

    // State
    public static ShinyProxyStatus status = ShinyProxyStatus.OFFLINE;

    // Shiny Proxy config
    public Path SHINYPROXY_CONFIG_DIRECTORY;
    // Template
    final private String SHINYPROXY_INTERNAL_DOCKERFILE_PATH = "shinyProxy/docker/Dockerfile";
    final private String SHINYPROXY_INTERNAL_TEMPLATE_PATH = "shinyProxy/templates/default/index.html";

    final private String SHINYPROXY_EXTERNAL_CONFIG_FILE_NAME = "application.yml";
    final private String SHINYPROXY_EXTERNAL_DOCKER_FILES_DIR = "docker";
    final private String SHINYPROXY_EXTERNAL_DOCKER_TEMPLATE_DIR = "templates/default";

    private final static String SHINYPROXY_CONTAINER_NAME = "opensilex-shinyproxy";
    final private String SHINYPROXY_DOCKER_IMAGE_NAME = "opensilex-shinyproxy";

    public final String SHINYPROXY_APP_DOCTYPE = "http://www.opensilex.org/vocabulary/oeso#ShinyAppPackage";

    public ShinyProxyServer(ShinyProxyConfig shinyProxyConfig) {
        super(shinyProxyConfig);
        config = shinyProxyConfig;
    }

    public URL serverUrl() {
        URL serverURL = null;
        try {
            if(!StringUtils.isBlank(config.publicServerURL()) ){
                serverURL = new URL(config.publicServerURL());
            }else{
                serverURL = new URL(config.httpScheme() + "://" + config.host() + ":" + Integer.toString(config.port()));
            } 
        } catch (MalformedURLException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return serverURL;
    }

    public ShinyProxyStatus getStatus() {
        return status;
    }

    public void setStatus(ShinyProxyStatus status) {
        ShinyProxyServer.status = status;
    }

    public void setupServer(SPARQLService sparql,FileStorageService fs) throws Exception {
        if (config.enable()) {
            status = ShinyProxyStatus.UPDATING;
            ScientificAppDAO scientificAppDAO = new ScientificAppDAO(sparql,fs);
            List<ScientificAppModel> allScientificAppModel = scientificAppDAO.getAll();

            LOGGER.info("Creating config directory ...");
            createConfigDirectory();
            LOGGER.info("Creating config file ...");
            ShinyProxyConfigYaml createdShinyProxyConfig = createShinyProxyConfigFromUserConfig(allScientificAppModel);
            LOGGER.debug(createdShinyProxyConfig.toString());
            LOGGER.info("Creating config app files ...");
            createDockerDirAndBuildDockerImages(allScientificAppModel,fs);

            LOGGER.info("Config path name " + SHINYPROXY_CONFIG_DIRECTORY.toString() + " file to " + SHINYPROXY_EXTERNAL_CONFIG_FILE_NAME);
//          // Copy application file
            Path applicationFilePath = Paths.get(SHINYPROXY_CONFIG_DIRECTORY.toString(), SHINYPROXY_EXTERNAL_CONFIG_FILE_NAME);
            LOGGER.info("Write config " + SHINYPROXY_EXTERNAL_CONFIG_FILE_NAME + " file to " + applicationFilePath);
            copyApplicationConfigToConfigDir(createdShinyProxyConfig, applicationFilePath);
            LOGGER.info("Internal Dockerfile  " + SHINYPROXY_INTERNAL_DOCKERFILE_PATH);
            InputStream dockerFileStream = getClass().getClassLoader().
                    getResourceAsStream(SHINYPROXY_INTERNAL_DOCKERFILE_PATH);
            LOGGER.info("Internal Dockerfile path to " + dockerFileStream);
            
            Path dockerFileFilePath = Paths.get(SHINYPROXY_CONFIG_DIRECTORY.toString(), "Dockerfile");
            LOGGER.info("Copy internal Dockerfile to " + dockerFileFilePath);
            FileUtils.copyResourcefileToDir(dockerFileStream, dockerFileFilePath);
            
            InputStream templateRessource = getClass().getClassLoader().
                    getResourceAsStream(SHINYPROXY_INTERNAL_TEMPLATE_PATH);
            LOGGER.info("Internal Dockerfile template to " + SHINYPROXY_INTERNAL_TEMPLATE_PATH);

            Path dockertemplateDirPath = Paths.get(SHINYPROXY_CONFIG_DIRECTORY.toString(),
                    SHINYPROXY_EXTERNAL_DOCKER_TEMPLATE_DIR
            );
            LOGGER.info("Creating template dir ...");
            dockertemplateDirPath.toFile().mkdirs();
            Path dockertemplateFilePath = Paths.get(SHINYPROXY_CONFIG_DIRECTORY.toString(),
                    SHINYPROXY_EXTERNAL_DOCKER_TEMPLATE_DIR,
                    "index.html"
            );
            LOGGER.info("Copy internal Dockerfile template to " + dockertemplateDirPath);
            FileUtils.copyResourcefileToDir(templateRessource, dockertemplateFilePath);

            LOGGER.info("Try to create network :" + config.shinyProxyNetwork());
            if (!DockerUtils.checkDockerNetworkExist(config.shinyProxyNetwork())) {
                DockerUtils.dockerCreateNetwork(config.shinyProxyNetwork());
            }

            LOGGER.info("Try to create image :" + getShinyProxyDockerImageName());
            if (!DockerUtils.checkDockerImageExist(getShinyProxyDockerImageName(), null)) {
                DockerUtils.dockerRemoveImage(getShinyProxyDockerImageName());
            }
            DockerUtils.dockerBuildImage(SHINYPROXY_CONFIG_DIRECTORY, getShinyProxyDockerImageName());
        }
    }

    /**
     * Represent ShinyProxy configuration // title: OpenSILEX Shiny Proxy proxy:
     * logo-url: http://opensilex.org/img/opensilex_logo.png # landing-page: / #
     * heartbeat-rate: 10000 # heartbeat-timeout: 60000 # bind-address:
     * 127.0.0.1 port: 8080 authentication: none hide-navbar: true # Docker
     * configuration docker: internal-networking: true specs: - id: 01_hello
     * display-name: Hello Application description: Application which
     * demonstrates the basics of a Shiny app container-cmd: ["R", "-e",
     * "shinyproxy::run_01_hello()"] container-image:
     * openanalytics/shinyproxy-demo container-network: shiny-proxy-network
     * logging: file: shinyproxy.log
     */
    private ShinyProxyConfigYaml createShinyProxyConfigFromUserConfig(List<ScientificAppModel> allScientificAppModel) {
        Proxy proxy = new Proxy();
        proxy.setTitle("OpenSILEX Shiny Proxy");
        proxy.setLogoUrl("http://opensilex.org/img/opensilex_logo.png");
        proxy.setPort(8080);
        proxy.setAuthentication("none");
        proxy.setTemplatePath("./templates/default");

        proxy.setHideNavbar(config.internalDockerConfig().hideNavbar());
        proxy.setContainerWaitTime(config.internalDockerConfig().containerWaitTime());
        proxy.setLandingPage(config.landingPage());

        Docker docker = new Docker();
        docker.setInternalNetworking(true);
        proxy.setDocker(docker);
        
        Servlet servlet = new Servlet();
        servlet.setContextPath(config.serverServletContextPath());
        Server server = new Server();
        server.setServlet(servlet);

        // TODO Add applications
        List<Spec> specs = new ArrayList<>();

        for (ScientificAppModel scientificAppModel : allScientificAppModel) {
            Spec spec = new Spec();
            spec.setId(scientificAppModel.getId());
            spec.setDisplayName(scientificAppModel.getName());
            spec.setDescription(scientificAppModel.getDescription());
//        List<String> containerCmd = Stream.of("R", "-e", "shinyproxy::run_01_hello()").collect(Collectors.toList());
//        spec.setContainerCmd(containerCmd);
            spec.setContainerNetwork(config.shinyProxyNetwork());
            spec.setContainerImage(getShinyProxyContainerName() + "-" + scientificAppModel.getId());
            specs.add(spec);

        }
        proxy.setSpecs(specs);

        ShinyProxyConfigYaml shinyProxyConfigYaml = new ShinyProxyConfigYaml();
        shinyProxyConfigYaml.setProxy(proxy);
        shinyProxyConfigYaml.setServer(server);

        return shinyProxyConfigYaml;
    }

    private void copyApplicationConfigToConfigDir(ShinyProxyConfigYaml createdShinyProxyConfig, Path applicationFilePath) throws ShinyProxyException {
        try {
            ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
            mapper.writeValue(applicationFilePath.toFile(), createdShinyProxyConfig);
            String json = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(createdShinyProxyConfig);
            LOGGER.info("Shiny proxy config : \n" + json);
        } catch (IOException ex) {
            LOGGER.error(ex.getMessage(), ex);
            throw new ShinyProxyException(ex.getMessage(), ex);
        }
    }

    public void startServer() throws Exception {
        if (config.enable()) {
            // If Docker service is running
            if (!DockerUtils.checkDockerIsRunning()) {
                throw new DockerException("Docker is not running");
            }
            // Run container
            if (DockerUtils.checkDockerContainerExist(getShinyProxyContainerName())) {
                LOGGER.info("Stop and remove container" + getShinyProxyContainerName());
                DockerUtils.dockerStopRemoveContainer(getShinyProxyContainerName());
            }

            List<String> dockerRunArgs = Stream.of("-d",
                    "-v",
                    "/var/run/docker.sock:/var/run/docker.sock",
                    "--net", config.shinyProxyNetwork(),
                    "--name", getShinyProxyContainerName(),
                    "-p",
                    config.port() + ":8080",
                    getShinyProxyDockerImageName())
                    .collect(Collectors.toList());

            if (!DockerUtils.dockerRun(dockerRunArgs)) {
                throw new DockerException("Container " + getShinyProxyContainerName() + " failed to start");
            }
            status = ShinyProxyStatus.ONLINE;
        }
    }

    private void createConfigDirectory() throws ShinyProxyException {
        Path configFilePath = null;
        if (config.configFilePath() == null
                || config.configFilePath().trim().isEmpty()) {
            try {
                configFilePath = Files.createTempDirectory("shinyProxyConfig_");
            } catch (IOException ex) {
                LOGGER.error("Can't create config directory ..." + ex.getMessage(), ex);
                throw new ShinyProxyException("Can't create config directory ...");

            }
        } else {
            configFilePath = Paths.get(config.configFilePath().trim());
        }

//       is dir
        if (Files.isDirectory(configFilePath)) {
//       is writable
            if (Files.isWritable(configFilePath)) {
                SHINYPROXY_CONFIG_DIRECTORY = configFilePath;
            } else {
                LOGGER.error("Config path is not a writable directory  ...");
                throw new ShinyProxyException("Config path is not a writable directory ...");
            }
        } else {
            LOGGER.error("Config path is not a directory  ...");
            throw new ShinyProxyException("Config path is not a directory ...");
        }
    }

    public void stopServer() {
        if (config.enable()) {
            DockerUtils.stopDockerContainer(getShinyProxyContainerName());
            status = ShinyProxyStatus.OFFLINE;
        }
    }

    public void reload(SPARQLService sparqlService,FileStorageService fs) throws Exception {
        if (config.enable()) {
            stopServer();
            setupServer(sparqlService, fs);
            startServer();
        }
    }

    /**
     * Create a directory to save informations to run dockerFile and scientific
     * applications
     *
     * @param scientificAppModels
     */
    private void createDockerDirAndBuildDockerImages(List<ScientificAppModel> scientificAppModels,FileStorageService fs) throws IOException, URISyntaxException {
        for (ScientificAppModel scientificAppModel : scientificAppModels) {
            URI uri = scientificAppModel.getUri();
            byte[] appByte = fs.readFileAsByteArray(ScientificAppDAO.FS_DATANALYSIS_PREFIX,new URI(SPARQLDeserializers.getExpandedURI(uri))) ;
            File document = File.createTempFile("app", "zip");
            org.apache.commons.io.FileUtils.writeByteArrayToFile(document, appByte);
            // docker config/docker/Id
            Path applicationDirPath = Paths.get(SHINYPROXY_CONFIG_DIRECTORY.toString(),
                    SHINYPROXY_EXTERNAL_DOCKER_FILES_DIR,
                    scientificAppModel.getId());
            // Create apps directory
            applicationDirPath.toFile().mkdirs();

            LOGGER.info("Unzipping " + scientificAppModel.getName() + " into " + applicationDirPath.toString());
            boolean unzipFile = FileUtils.unzipFile(document.getAbsolutePath(), applicationDirPath.toString());
            if (!unzipFile) {
                LOGGER.error("Can't unzip file : " + scientificAppModel.getName() + " into " + applicationDirPath.toString());
            } else {
                buildDockerImage(applicationDirPath, scientificAppModel);
            }

        }
    }

    /**
     * Create docker image from application definition
     *
     * @param shinyProxyAppList Application definition
     */
    private boolean buildDockerImage(Path applicationDirPath, ScientificAppModel scientificAppModel) {
        return DockerUtils.dockerBuildImage(applicationDirPath, getShinyProxyContainerName() + "-" + scientificAppModel.getId());
    }
    
    public String getShinyProxyContainerName(){
        return config.shinyProxyContainerPrefix() + "-" + SHINYPROXY_CONTAINER_NAME;
    }
    public String getShinyProxyDockerImageName(){
        return config.shinyProxyContainerPrefix() + "-" + this.SHINYPROXY_DOCKER_IMAGE_NAME;
    }

}
