package org.opensilex.dataAnalysis.shinyProxy.config;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "title",
    "logo-url",
    "landing-page",
    "port",
    "template-path",
    "authentication",
    "hide-navbar",
    "docker",
    "specs"
})
public class Proxy {

    @JsonProperty("title")
    private String title;
    @JsonProperty("logo-url")
    private String logoUrl;
    @JsonProperty("landing-page")
    private String landingPage;
    @JsonProperty("port")
    private Integer port;
    @JsonProperty("template-path")
    private String templatePath;
    @JsonProperty("authentication")
    private String authentication;
    @JsonProperty("hide-navbar")
    private Boolean hideNavbar;
    @JsonProperty("docker")
    private Docker docker;
    @JsonProperty("container-wait-time")
    private Integer containerWaitTime;
    @JsonProperty("specs")
    private List<Spec> specs = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("title")
    public String getTitle() {
        return title;
    }

    @JsonProperty("title")
    public void setTitle(String title) {
        this.title = title;
    }

    @JsonProperty("logo-url")
    public String getLogoUrl() {
        return logoUrl;
    }

    @JsonProperty("logo-url")
    public void setLogoUrl(String logoUrl) {
        this.logoUrl = logoUrl;
    }

    @JsonProperty("port")
    public Integer getPort() {
        return port;
    }

    @JsonProperty("port")
    public void setPort(Integer port) {
        this.port = port;
    }

    @JsonProperty("authentication")
    public String getAuthentication() {
        return authentication;
    }

    @JsonProperty("authentication")
    public void setAuthentication(String authentication) {
        this.authentication = authentication;
    }

    @JsonProperty("hide-navbar")
    public Boolean getHideNavbar() {
        return hideNavbar;
    }

    @JsonProperty("hide-navbar")
    public void setHideNavbar(Boolean hideNavbar) {
        this.hideNavbar = hideNavbar;
    }

    @JsonProperty("docker")
    public Docker getDocker() {
        return docker;
    }

    @JsonProperty("docker")
    public void setDocker(Docker docker) {
        this.docker = docker;
    }

    @JsonProperty("specs")
    public List<Spec> getSpecs() {
        return specs;
    }

    @JsonProperty("specs")
    public void setSpecs(List<Spec> specs) {
        this.specs = specs;
    }

    @JsonProperty("landing-page")
    public String getLandingPage() {
        return landingPage;
    }

    @JsonProperty("landing-page")
    public void setLandingPage(String landingPage) {
        this.landingPage = landingPage;
    }

    @JsonProperty("template-path")
    public String getTemplatePath() {
        return templatePath;
    }

    @JsonProperty("template-path")
    public void setTemplatePath(String templatePath) {
        this.templatePath = templatePath;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @JsonProperty("container-wait-time")
    public Integer getContainerWaitTime() {
        return containerWaitTime;
    }

    @JsonProperty("container-wait-time")
    public void setContainerWaitTime(Integer containerWaitTime) {
        this.containerWaitTime = containerWaitTime;
    }
}
