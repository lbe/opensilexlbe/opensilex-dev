/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.opensilex.dataAnalysis.shinyProxy;

import org.opensilex.config.ConfigDescription;

/**
 *
 * @author charlero
 */
public interface DockerConfig {
    @ConfigDescription(
            value = "internalNetworking parameter",
            defaultBoolean = true
    )
    Boolean internalNetworking(); 
}
