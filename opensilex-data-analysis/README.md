# Introduction

This module aims to propose tools in order to deploy applications in R or Python. It also permit to execute R code using OpenCPU.

## Prerequisites

- **Unix system**
- **Admin rights**


## Miscellaneous

Due to "ShinyProxy experienced an unrecoverable error." the  shiny proxy dockerfile has been downgrade to version 2.6.1 .
See tag 2.4.1.


## Installation steps

1. Install docker

```bash
curl -fsSL https://get.docker.com -o get-docker.sh | sh
```

2. Setup docker (Need admin rights)

```bash
# add active user to docker group
sudo usermod -aG docker $USER
```

**For Shiny proxy :**

- Use the command `sudo systemctl edit docker.service` to open an override file for docker.service in a text editor.

- Add or modify the following lines, substituting your own values.

```bash
[Service]
ExecStart=
ExecStart=/usr/bin/dockerd -H fd:// -H tcp://127.0.0.1:2375
```

- Save the file and reload the systemctl configuration.

```bash
sudo systemctl daemon-reload
```

- Restart Docker service.

```bash
sudo systemctl restart docker.service
```

- Check to see whether the change was honored by reviewing the output of netstat to confirm dockerd is listening on the configured port.

```bash
sudo netstat -lntp | grep dockerd
tcp        0      0 127.0.0.1:2375          0.0.0.0:*               LISTEN      3758/dockerd
```

For more information : [See shinyproxy docker startup installation](<[https://link](https://www.shinyproxy.io/getting-started/#docker-startup-options)>) and
[Configuring docker remote access](<[https://link](https://docs.docker.com/engine/install/linux-postinstall/#configuring-remote-access-with-systemd-unit-file)>)

1. Add Minimal opensilex datanalysis configuration yml file.

```yaml
data-analysis:
  # ShinyProxyServerDocker config (ShinyProxyServer)
  scientificAppServer:
    config:
      # Host parameter (String)
      host: localhost
      # Port parameter (Integer)
      port: 8098
      # Activate or deactivate shiny proxy server (Boolean)
      enable: true
      # Public server URL parameter (String)
     # publicServerURL:
      # Http scheme parameter (String)
     #  httpScheme: http
      # Docker network name (String)
      shinyProxyNetwork: os-project-shiny-proxy-network
      # Proxy config (InternalConfig)
    # internalDockerConfig:
        # Authentication parameter (String)
    #   authentication: none
        # title (Boolean)
    #   hideNavbar: true
        # Container waiting time (Integer)
    #   containerWaitTime: 60000
        # title (String)
    #    title: OpenSILEX Shiny Proxy
        # title (String)
    #   logoUrl:
      # landing page parameter (String)
    #  landingPage: / # for proxy as nginx
      # server.servlet.context-path (String)
    #  serverServletContextPath: /  # for proxy as nginx
      # Docker container prefix name (multiple shinyproxy on same server) (String)
      shinyProxyContainerPrefix: os-project
 :
  # OpencpuServer config (OpencpuServer)
#  rServer:
#    # Service implementation class for: rServer (OpencpuServer)
#    implementation: org.opensilex.dataAnalysis.opencpu.OpencpuServer
#    config:
#      # Host parameter (String)
#      host: localhost
#      # Port parameter (Integer)
#      port: 8098
#      # Activate or deactivate shiny proxy server (Boolean)
#      enable: false
#      # Docker image tag (String)
#      dockerImageTag: 2.1.6
#      # Docker image (String)
#      dockerImage: opencpu/rstudio
#      # Http scheme parameter (String)
#      httpScheme: http
```

## Start with an example

- Download an application : [Shiny App example](https://forgemia.inra.fr/OpenSILEX/data-analysis-visualisation/opensilex-data-analysis/-/raw/master/src/main/resources/ExampleApp/shinyAppShinyProxy2.61.zip)

It contains a Dockerfile that is mandatory and the app needed by the Dockerfile in order to be deployed.
```
.
├── app
│   └── app.R
└── Dockerfile
```

- Go to the interface fill the form and restart application server by clicking in the menu.


## Configure reverse proxy 

Example : I want shinyproxy to be accessible from https://{serveraddress}/shinyproxy

* OpenSILEX configuration :
```

data-analysis:
     scientificAppServer:
         config:
            enable: true
            host: 127.0.0.1
            port: 8004
            publicServerURL : https://{serveraddress}/shinyproxy
            serverServletContextPath : /shinyproxy         
            # Docker container prefix name (multiple shinyproxy on same server) (String)
            shinyProxyContainerPrefix: opensilex-shinyproxy
            # Docker network name (String)
            shinyProxyNetwork: opensilex-shiny-proxy-network


```

* Nginx configuration
```
        location /shinyproxy/ {
                # First attempt to serve request as file, then
                # as directory, then fall back to displaying a 404.
                #try_files $uri $uri/ =404;

                proxy_pass http://127.0.0.1:8004/shinyproxy/;
                proxy_http_version 1.1;
                proxy_set_header Upgrade $http_upgrade;
                proxy_set_header Connection "upgrade";
                proxy_read_timeout 5000s;

                proxy_redirect    off;
                proxy_set_header  Host              $http_host;
                proxy_set_header  X-Real-IP         $remote_addr;
                proxy_set_header  X-Forwarded-For   $proxy_add_x_forwarded_for;
                proxy_set_header  X-Forwarded-Proto $scheme;
                proxy_set_header X-NginX-Proxy true;
        }

```

## Migrate for before metadata functionnality from version before 1.1.0 of opensilex

```sparql
PREFIX terms: <http://purl.org/dc/terms/>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
DELETE {
   GRAPH <http://opensilex.vitioeno/set/shinyApps>{
	  ?uri terms:modified  ?existingTextM .
    ?uri terms:created  ?existingTextC .
  }
}
INSERT{
  GRAPH <http://opensilex.vitioeno/set/shinyApps>{
	  ?uri terms:modified  ?datetimeM .
    ?uri terms:created  ?datetimeC .
  }
}
WHERE {
  GRAPH <http://opensilex.vitioeno/set/shinyApps>{
  	?uri terms:modified  ?existingTextM .
    ?uri terms:created  ?existingTextC .

    BIND(CONCAT(str(?existingTextM), "T00:00:00Z") AS ?appendedTextM)
    BIND(CONCAT(str(?existingTextC), "T00:00:00Z") AS ?appendedTextC)

    BIND(strdt(str(?appendedTextM), xsd:dateTime) AS ?datetimeM)
    BIND(strdt(str(?appendedTextC), xsd:dateTime) AS ?datetimeC)
  }
}
```

## Migrate from version 2.X to 3.0.0 of this module

```sparql
PREFIX  oeso: <http://www.opensilex.org/vocabulary/oeso-ext#>
PREFIX  vitioeno-ressource-grp: <http://opensilex.vitioeno/set/group#>
PREFIX  vitioeno-device: <http://opensilex.vitioeno/set/devices#>
PREFIX  owl:  <http://www.w3.org/2002/07/owl#>
PREFIX  skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX  vitioeno-oowl-ext: <http://opensilex.vitioeno/opensilex-owl-extension#>
PREFIX  vitioeno-doc: <http://opensilex.vitioeno/set/documents#>
PREFIX  vitioeno-ressource-so: <http://opensilex.vitioeno/set/scientific-object#>
PREFIX  iado: <http://www.opensilex.org/vocabulary/iado#>
PREFIX  vitioeno-ressource-person: <http://opensilex.vitioeno/set/user#>
PREFIX  peco: <http://purl.obolibrary.org/obo/peco.owl#>
PREFIX  vitioeno-ressource-device: <http://opensilex.vitioeno/set/device#>
PREFIX  dcterms: <http://purl.org/dc/terms/#>
PREFIX  foaf: <http://xmlns.com/foaf/0.1/>
PREFIX  vitioeno-area: <http://opensilex.vitioeno/set/area#>
PREFIX  vitioeno-usr: <http://opensilex.vitioeno/users#>
PREFIX  vitioeno-ScientificApplication: <http://opensilex.vitioeno/set/shinyApps#>
PREFIX  vcard: <http://www.w3.org/2006/vcard/ns#>
PREFIX  vitioeno-ressource-usr: <http://opensilex.vitioeno/set/user#>
PREFIX  vitioeno-prf: <http://opensilex.vitioeno/profiles#>
PREFIX  oeev: <http://www.opensilex.org/vocabulary/oeev#>
PREFIX  vitioeno-ressource-oowl-ext: <http://opensilex.vitioeno/set/opensilex-owl-extension#>
PREFIX  vitioeno-ressource-germplasm: <http://opensilex.vitioeno/set/germplasm#>
PREFIX  vitioeno-ressource-account: <http://opensilex.vitioeno/set/user#>
PREFIX  sesame: <http://www.openrdf.org/schema/sesame#>
PREFIX  os-sec: <http://www.opensilex.org/security#>
PREFIX  vitioeno-ressource-ScientificApplication: <http://opensilex.vitioeno/set/shinyApps#>
PREFIX  vitioeno-prj: <http://opensilex.vitioeno/set/projects#>
PREFIX  vitioeno-germplasm: <http://opensilex.vitioeno/germplasm#>
PREFIX  vitioeno-so: <http://opensilex.vitioeno/scientific-objects#>
PREFIX  object: <http://www.mistea.supagro.inra.fr/object#>
PREFIX  vitioeno-ressource-prf: <http://opensilex.vitioeno/set/profile#>
PREFIX  xsd:  <http://www.w3.org/2001/XMLSchema#>
PREFIX  fn:   <http://www.w3.org/2005/xpath-functions#>
PREFIX  rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX  vitioeno-ressource-orga: <http://opensilex.vitioeno/set/organization#>
PREFIX  vitioeno-ressource-prj: <http://opensilex.vitioeno/set/project#>
PREFIX  vitioeno-factor: <http://opensilex.vitioeno/set/factors#>
PREFIX  geo:  <http://www.w3.org/2003/01/geo/wgs84_pos#>
PREFIX  oa:   <http://www.w3.org/ns/oa#>
PREFIX  vocabulary: <http://www.opensilex.org/vocabulary/oeso#>
PREFIX  vitioeno: <http://opensilex.vitioeno/>
PREFIX  opensilex-api: <http://www.opensilex.org/vocabulary/opensilex-api#>
PREFIX  vitioeno-ressource-expe: <http://opensilex.vitioeno/set/experiment#>
PREFIX  vitioeno-ressource-doc: <http://opensilex.vitioeno/set/document#>
PREFIX  prov: <http://www.w3.org/ns/prov#>
PREFIX  rdf4j: <http://rdf4j.org/schema/rdf4j#>
PREFIX  org:  <http://www.w3.org/ns/org#>
PREFIX  oeso-analysis: <https://www.opensilex.org/vocabulary/oeso/analysis#>
PREFIX  vitioeno-ev: <http://opensilex.vitioeno/events#>
PREFIX  vitioeno-grp: <http://opensilex.vitioeno/groups#>
PREFIX  vitioeno-expe: <http://opensilex.vitioeno/set/experiments#>
PREFIX  vitioeno-infra: <http://opensilex.vitioeno/infrastructures#>
PREFIX  vitioeno-ressource-infra: <http://opensilex.vitioeno/set/organization#>
PREFIX  rdf:  <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX  vitioeno-ressource-factor: <http://opensilex.vitioeno/set/factor#>
PREFIX  time: <http://www.w3.org/2006/time#>
PREFIX  vitioeno-ressource-area: <http://opensilex.vitioeno/set/area#>
PREFIX  vitioeno-ressource: <http://opensilex.vitioeno/>
PREFIX  agrovoc: <http://aims.fao.org/aos/agrovoc/factors#>
PREFIX  dc:   <http://purl.org/dc/terms/>
PREFIX  vitioeno-vocabulary: <http://vitioeno.inrae.fr/vocabulary#>
insert {
  graph ?g {
    ?uri <http://www.opensilex.org/vocabulary/oeso/analysis#hasProgrammingLanguage> "NA"
  }
} where {
  graph ?g {
    ?uri ?p ?o
  }
  VALUES ?g { <http://opensilex.vitioeno/set/shinyApps>}
}
```
